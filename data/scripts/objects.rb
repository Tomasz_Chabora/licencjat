class Pearls < Entity
  def initialize
    @amount = 500
  end
  
  def init(x, y)
    @x = x * Tile::SIZE
    @y = y * Tile::SIZE
    return if $state.get_collected(@x, @y)
    @collider = BoxCollider.new(@x, @y, 40, 40)
    super()
  end
  
  def update
    if $state.player.collider.collides?(@collider)
      Label.new(:item, text: "500p")
      $state.add_collected(@x, @y)
      remove
    end
  end
  
  def draw
    img("Objects/#{@amount}p").draw(@x, @y, 1)
  end
end

class Item < Entity
  attr_reader :name
  
  def init(x, y)
    @x = x * Tile::SIZE
    @y = y * Tile::SIZE
    return if $state.get_collected(@x, @y)
    @collider = BoxCollider.new(@x, @y, 40, 40)
    super()
  end
  
  def update
    if $state.player.collider.collides?(@collider)
      snd("Item").play
      Label.new(:item, text: @name)
      $state.add_collected(@x, @y)
      remove
      $save[:inventory] << self.to_inventory
    end
  end
  
  def draw(x = @x, y = @y, z = 1)
    @icon.each do |layer|
      img("Objects/Items/#{layer[:graphic]}").draw(x, y, z, 1, 1, Color.new(*layer[:color].collect{|v| [v*16, 255].min}), layer[:mode])
    end
  end
  
  def to_inventory
    remove_instance_variable(:@x)
    remove_instance_variable(:@y)
    remove_instance_variable(:@collider)
    remove_instance_variable(:@_tags)
    remove_instance_variable(:@_spawn_time)
    self
  end
  
  def base
    ItemBase.get(@id)
  end
  
  def self.free(x, y)
    $state.map.grid[[x/Tile::SIZE, y/Tile::SIZE]] == :air
  end
  
  def self.de_wall
    if !free(@x, @y)
      10.times do |i|
        [U, R, L].each do |dir|
          x = @x + {U => 0, R => 1, L => -1} * Tile::SIZE
          y = @y + {U => -1, R => 0, L => 0} * Tile::SIZE
          
          if free(x, y)
            @x, @y = x, y
            return
          end
        end
      end
    end
  end
end

class Relic < Entity
  ABILITIES = :double_jump, :slide, :wall_jump, :dash, :sinking, :super_jump, :water_walking, :water_breathing
  NAMES = {double_jump: "Air-repulsion Boots", slide: "Never-ending Jar of Slime", wall_jump: "Honeyed Gloves", dash: "Silver Wings", sinking: "Lead Stone", super_jump: "Non-explosive Firework", water_walking: "Hydrophobic Soles", water_breathing: "Diving Mask"}
  SHORT_NAMES = {double_jump: "djump", slide: "slide", wall_jump: "wjump", dash: "dash", sinking: "sink", super_jump: "sjump", water_walking: "wwalk", water_breathing: "breath"}
  DESCRIPTIONS = {double_jump: "Press Jump in mid-air for a second jump.", slide: "Press Down + Special to slide through narrow passages.", wall_jump: "Press Jump while falling against wall to bounce off.", dash: "Double tap direction to start running.", sinking: "Sink in the water.", super_jump: "Press Up + Special while crouching to do a high jump.", water_walking: "Walk on water surface.", water_breathing: "Stay infinitely underwater."}
  
  attr_reader :type
  
  def initialize(type)
    @type = type
  end
  
  def init(x, y)
    @x = x * Tile::SIZE
    @y = y * Tile::SIZE
    return if $state.get_collected(@x, @y)
    @collider = BoxCollider.new(@x, @y, 40, 40)
    super()
  end
  
  def update
    if $state.player.collider.collides?(@collider)
      snd("Relic").play
      $state.info(SpecialInfo.new(NAMES[@type], DESCRIPTIONS[@type]))
      $save[:abilities] << @type
      $state.add_collected(@x, @y)
      remove
    end
  end
  
  def draw
    tls("Objects/Relics", 40)[ABILITIES.index(@type)].draw(@x, @y, 1)
  end
end

class Booster < Entity
  def initialize(type)
    @type = type
  end
  
  def init(x, y)
    @x = x * Tile::SIZE
    @y = y * Tile::SIZE
    return if $state.get_collected(@x, @y)
    @collider = BoxCollider.new(@x, @y, 40, 40)
    super()
  end
  
  def update
    if $state.player.collider.collides?(@collider)
      snd("Relic").play
      case @type
        when 0
        $state.info(SpecialInfo.new("Golden heart", "Max health +50"))
        $save[:hp] = ($save[:max_hp] += 50)
      end
      $state.add_collected(@x, @y)
      remove
    end
  end
  
  def draw
    tls("Objects/Boosters", 40)[@type].draw(@x, @y, 1)
  end
end

class Save < Entity
  def initialize(x, y)
    @x = x * Tile::SIZE - 80
    @y = y * Tile::SIZE - 120
    
    init
  end
  
  def update
    if @warp and @warp.destination
      @warping ||= 0
      @dwarping ||= 0
      
      @dwarping += 1 if @dwarping < 120
      @warping += @dwarping/4
      $state.player._no_update = true
      
      if @warping > 2000
        last_location = $state.map.room.area.location
        $state.reset
        $state.player.init
        Map.new(@warp.destination)
        stop_music if $state.map.room.area.location != last_location
        
        save_point = $state.find{|ent| ent.class == Save}
        save_point.dewarp
        $state.player.x = save_point.x + 30
        $state.player.y = save_point.y + 60
      end
      
      return
    elsif @dewarp
      @dwarping -= 1 if @dwarping > 4
      @warping += @dwarping/4
      
      if @dwarping == 4 and @warping % 360 == 0
        @dewarp = @dwarping = @warping = nil
        $state.player._no_update = nil
      end
      
      return
    end
    
    if is_player? and key_press(:up)
      snd("Relic").play
      $save[:save_point] = [$state.map.room.x, $state.map.room.y]
      $save[:hp] = $save[:max_hp]
      
      save = File.new("data/save", 'w')
      Marshal.dump($save, save)
      Marshal.dump($state.world, save)
      save.close
      
      Trail.new(@x, @y, ["Effects/Save", 160, 160], animation: 4, z: 2)
    elsif is_player? and key_press(:down)
      snd("MenuOpen").play
      $state.info(@warp = MapScreen.new($state.world, *$state.get_map_square).warp_mode)
    end
  end
  
  def draw
    img("Objects/SaveBase").draw(@x, @y, 0)
    if @warping
      img("Objects/Save").draw_rot(@x + 80, @y + 80, 0, -@warping)
      return
    else
      img("Objects/Save").draw(@x, @y, 0)
    end
    
    if is_player?
      img("System/ActionArrow").draw(@x, @y - 40, HUD_Z)
      fnt("Courier New", 32).draw("Save", @x + 42, @y - 38, HUD_Z, 1, 1, Color::WHITE)
      fnt("Courier New", 32).draw("Save", @x + 40, @y - 40, HUD_Z, 1, 1, Color::RED)
      img("System/ActionArrow").draw(@x, @y + 200, HUD_Z, 1, -1)
      fnt("Courier New", 32).draw("Teleport", @x + 42, @y + 170, HUD_Z, 1, 1, Color::WHITE)
      fnt("Courier New", 32).draw("Teleport", @x + 40, @y + 168, HUD_Z, 1, 1, Color::RED)
    end
  end
  
  def is_player?
    $state.player.left > @x && $state.player.right < @x+160 && $state.player.top > @y && $state.player.bottom < @y+180
  end
  
  def dewarp
    @dewarp = true
    @dwarping = 120
    @warping = 50
  end
end