class Connection
  attr_reader :grid, :type
  
  def initialize
    @grid = Array.new(SegmentBase::WIDTH * SegmentBase::HEIGHT) {nil}
    @modifiers = []
    @type = 0
  end
  
  def get_pattern(direction)
    if mod = has_modifier?("override pattern")
      return mod.split.last
    end
    
    return if @grid.empty?
    case direction
      when U
      @grid[0...SegmentBase::WIDTH]
      when R
      ((SegmentBase::WIDTH-1)..(SegmentBase::WIDTH*SegmentBase::HEIGHT-1)).step(SegmentBase::WIDTH).map{|i| @grid[i]}
      when D
      w = SegmentBase::WIDTH
      h = SegmentBase::HEIGHT
      @grid[(w*h-w)...(w*h)]
      when L
      (0...(SegmentBase::WIDTH*SegmentBase::HEIGHT-1)).step(SegmentBase::WIDTH).map{|i| @grid[i]}
    end.map{|t| t == 2 ? 0 : [nil,3,4,5,6].include?(t) ? 1 : t}
  end
  
  def has_modifier?(name)
    @modifiers.find{|mod| mod.start_with?(name)}
  end
end

class SegmentLayer
  T_TREASURE = 0
  T_ENEMY = 1
  T_DECORATION = 2
  T_SECONDARY = 3
  T_SAVE = 4
  
  attr_reader :type, :modifiers, :grid
  
  def initialize(type, modifiers, grid)
    @type, @grid = type, grid
    @modifiers = modifiers.split(/;\s*/)
  end
  
  def has_modifier?(name)
    @modifiers.find{|mod| mod.start_with?(name)}
  end
end

class SegmentBase
  attr_reader :grid, :layers
  
  def initialize
    @grid = Array.new(WIDTH * HEIGHT) {0}
    @connections = Array.new(4) {[]}
    @layers = []
    @modifiers = []
  end
  
  def get_connections(direction)
    @connections[direction]
  end
  
  def self.get(id)
    raise "No segment with id: #{id}" if !@@segments[id]
    @@segments[id]
  end
  
  def self.count
    @@segments.length
  end
  
  def has_modifier?(name)
    @modifiers.find{|mod| mod.start_with?(name)}
  end
  
  @@segments = []
  (Dir.entries("data/base/Segments") - ['.','..']).each do |id|
    @@segments << File.open("data/base/Segments/#{id}") {|file| Marshal.load(file)}
  end
end

class RoomBase
  attr_reader :min_width, :min_height, :max_width, :max_height, :segments
  attr_reader :vertical_exit, :horizontal_exit, :background
  
  def initialize
    @background = -1
    @min_width = @min_height = @max_width = @max_height = 1
    @vertical_exit = @horizontal_exit = 500
    @segments = []
    @modifiers = []
  end
  
  def self.get(id)
    raise "No room with id: #{id}" if !@@rooms[id]
    @@rooms[id]
  end
  
  def self.count
    @@rooms.length
  end
  
  def has_modifier?(name)
    @modifiers.find{|mod| mod.start_with?(name)}
  end
  
  @@rooms = []
  (Dir.entries("data/base/Rooms") - ['.','..']).each do |id|
    @@rooms << File.open("data/base/Rooms/#{id}") {|file| Marshal.load(file)}
  end
end

class AreaBase
  attr_reader :min_width, :min_height, :max_width, :max_height, :rooms
  attr_reader :connects, :enemies, :tilesets
  
  def initialize
    @min_width = @min_height = @max_width = @max_height = 1
    @rooms = []
    @connects = []
    @enemies = []
    @tilesets = []
  end
  
  def self.get(id)
    raise "No area with id: #{id}" if !@@areas[id]
    @@areas[id]
  end
  
  def self.count
    @@areas.length
  end
  
  def self.has_con_mod?(connection, name)
    connection[:modifiers].find{|mod| mod.start_with?(name)}
  end
  
  def has_modifier?(name)
    @modifiers.find{|mod| mod.start_with?(name)}
  end
  
  @@areas = []
  (Dir.entries("data/base/Areas") - ['.','..']).each do |id|
    @@areas << File.open("data/base/Areas/#{id}") {|file| Marshal.load(file)}
  end
end

class LocationBase
  attr_reader :areas, :names, :replacements, :color, :music
  
  def initialize
    @areas = []
    @names = []
    @replacements = []
  end
  
  def self.get(id)
    raise "No location with id: #{id}" if !@@locations[id]
    @@locations[id]
  end
  
  def self.count
    @@locations.length
  end
  
  @@locations = []
  (Dir.entries("data/base/Locations") - ['.','..']).each do |id|
    @@locations << File.open("data/base/Locations/#{id}") {|file| Marshal.load(file)}
  end
end

class BackgroundLayer
  P_BOTTOM = 0
  P_TOP = 1
  P_FILL = 2
  
  attr_reader :image, :position, :scroll_x, :scroll_y, :loop_x, :loop_y
  
  def initialize(image, position, scroll_x, scroll_y, loop_x, loop_y)
    @image = image
    @position, @scroll_x, @scroll_y, @loop_x, @loop_y = position, scroll_x, scroll_y, loop_x, loop_y
  end
end

class BackgroundBase
  attr_reader :layers
  
  def initialize
    @layers = []
  end
  
  def self.get(id)
    raise "No background with id: #{id}" if !@@backgrounds[id]
    @@backgrounds[id]
  end
  
  def self.count
    @@backgrounds.length
  end
  
  @@backgrounds = []
  (Dir.entries("data/base/Backgrounds") - ['.','..']).each do |id|
    @@backgrounds << File.open("data/base/Backgrounds/#{id}") {|file| Marshal.load(file)}
  end
end

class ItemBase
  TYPES = [:consumable, :misc, :weapon, :armor, :helmet, :boots, :accessory]
  
  attr_reader :type, :names, :replacements, :layers
  
  def initialize
    @type = :consumable
    @replacements = []
    @names = []
    @layers = []
  end
  
  def self.get(id)
    raise "No item with id: #{id}" if !@@items[id]
    @@items[id]
  end
  
  def self.count
    @@items.length
  end
  
  @@items = []
  (Dir.entries("data/base/Items") - ['.','..']).each do |id|
    @@items << File.open("data/base/Items/#{id}") {|file| Marshal.load(file)}
  end
end