class SegmentBase
  attr_accessor :modifiers, :layers
  
  def self.refresh
    @@segments = []
    (Dir.entries("data/base/Segments") - ['.','..']).each do |id|
      @@segments << Marshal.load(f = File.new("data/base/Segments/#{id}"))
      f.close
    end
  end
end

class Connection
  attr_accessor :type, :modifiers
end

class SegmentEdit
  LAYER_TYPES = "Item", "Enemy", "Decoration", "Alt. Tiles", "Save"
  
  def initialize
    @return = GUI::Button.new(720, 4, "Return")
    @save = GUI::Button.new(4, 4, "Save")
    GUI::Label.new(48, 4, "Segment " + $segment_id.to_s.rjust(4, "0"))
    @name = GUI::Textbox.new(192, 4, 32)
    @name.value = Metadata.load(:segment_name, $segment_id)
    
    GUI::Label.new(4, 400, "Modifiers")
    @modifiers = GUI::Textbox.new(4, 420, 64)
    @modifiers.value = $segment.modifiers.join("; ")
    
    GUI::Label.new(500, 64, "Layers")
    @add_layer = GUI::Button.new(600, 64, "+")
    @delete_layer = GUI::Button.new(640, 64, "-")
    
    @layer_edits = []
    $segment.layers.each.with_index do |layer, i|
      @layer_edits << new_layer_edit(i)
      @layer_edits.last[0].value = layer.type
      @layer_edits.last[1].value = layer.modifiers.join("; ")
      @layer_edits.last[2].value = layer.grid
    end
    
    @tiles = []
    sp = []
    tiles = ["Air", "Wall", "Platform", "UL Slope", "UR Slope", "DL Slope", "DR Slope", "Water"]#, "Lava"] #◢◣◥◤
    tiles.each_index do |i|
      @tiles << GUI::Button.new(700, 64+i*32, tiles[i], sp)
    end
    sp << @tiles.first
    @tiles.first.selected = true
    
    sp = []
    @connedits = []
    @connedits[U] = GUI::Button.new(256, 520, "Top connection (0)", sp)
    @connedits[R] = GUI::Button.new(440, 550, "Right connection (0)", sp)
    @connedits[D] = GUI::Button.new(240, 580, "Bottom connection (0)", sp)
    @connedits[L] = GUI::Button.new(40, 550, "Left connection (0)", sp)
    @connedits << GUI::Button.new(280, 550, "Normal edit", sp)
    sp << @connedits.last
    @connedits.last.selected = true
    
    @connedit = []
    @connedit << GUI::Spinner.new(660, 420, 0, 0) #0
    @connedit << GUI::Button.new(660, 448, "+") #1
    @connedit << GUI::Button.new(740, 448, "-") #2
    @connedit << GUI::Label.new(720, 420, "/ 0") #3
    @connedit << GUI::Dropdown.new(660, 480, "In-room", "Exit", "Both") #4
    @connedit << GUI::Textbox.new(4, 464, 64) #5
    @connedit << GUI::Label.new(660, 396, "Edit connection")
    @connedit << GUI::Label.new(4, 440, "Connection modifiers")
    
    @edit_connection = nil
    
    @preview = SegmentPreview.new($segment.grid, 4, 32)
    @connector = SegmentPreview.new([], 4, 32)
    @current_tile = 0
  end
  
  def new_layer_edit(i)
    [GUI::Dropdown.new(500, 92 + i*48, *LAYER_TYPES),
        GUI::Textbox.new(500, 92 + i*48+24, 21),
        GUI::ContainerButton.new(640, 92 + i*48, "Edit")]
  end
  
  def update
    if @return.value
      GUI::System.Clear
      SegmentBase.refresh
      
      return $state=SegmentBrowser.new
    end
    
    save if @save.value
    
    @tiles.each_index do |i|
      if @tiles[i].value
        @current_tile = i
        break
      end
    end
    
    [U, R, D, L].each do |d| @connedits[d].text.gsub!(/[0-9]/, $segment.get_connections(d).length.to_s)
      if @connedits[d].value
        @edit_connection = d
        @connedit[0].value = 0
        @connedit[0].max = $segment.get_connections(d).length
      end
    end
    
    @edit_connection = @edit_layer = nil if @connedits.last.value
    @connedit.each{|ce| ce.disabled = !@edit_connection}
    
    @layer_edits.each do |le|
      @edit_layer = le if le.last.value2
    end
    
    if @edit_layer
      @connector.grid = @edit_layer.last.value
      @connector.edit(@edit_layer.first.value2)
      @preview.darken = true
    elsif @edit_connection
      if @connedit[1].value
        sgce.push Connection.new
      elsif @connedit[2].value
        sgce.delete_at @connedit[0].value-1
      end
      
      if @connedit[0].value == 0 and sgce.length > 0
        @connedit[0].min = 1
      elsif sgce.length == 0
        @connedit[0].min = 0
        @connector.grid = []
      end
      @connedit[0].max = sgce.length
      @connedit[3].text = "/ #{@connedit[0].max}"
      
      if @connedit[0].value > 0
        last_grid = @connector.grid
        @connector.grid = sgce[@connedit[0].value-1].grid
        @connector.edit(@current_tile)
        
        if @connector.grid != last_grid
          @connedit[4].value = sgce[@connedit[0].value-1].type
          @connedit[5].value = sgce[@connedit[0].value-1].modifiers.join("; ")
        end
        sgce[@connedit[0].value-1].type = @connedit[4].value
        sgce[@connedit[0].value-1].modifiers = @connedit[5].value.split(/;\s*/)
      end
      @preview.darken = true
    else
      @preview.edit(@current_tile)
    end
    
    if @add_layer.value
      @layer_edits << new_layer_edit(@layer_edits.length)
     end
      
    if @delete_layer.value and !@layer_edits.empty?
      @layer_edits.last.each{|e| e.remove}
      @layer_edits.pop
      @edit_layer = nil
    end
  end
  
  def draw
    @preview.draw
    @connector.draw if @edit_connection or @edit_layer
    draw_line(244, 32, c = 0x40000000, 244, 382, c, 3)
    draw_line(245, 32, c, 245, 382, c, 3)
  end
  
  def save
    Metadata.store(:segment_name, $segment_id, @name.value)
    $segment.modifiers = @modifiers.value.split(/;\s*/)
    $segment.layers = @layer_edits.collect{|l| SegmentLayer.new(*l.collect{|ll| ll.value})}
    
    f = File.new("data/base/Segments/" + $segment_id.to_s.rjust(4, "0"), 'w')
    Marshal.dump($segment, f)
    f.close
  end
  
  def sgce
    $segment.get_connections(@edit_connection)
  end
end

class SegmentBrowser
  @@scroll = 0
  
  def initialize
    @segments = []
    @edits = []
    @labels = []
    
    (SegmentBase.count+1).times do |i|
      x = i%3*260
      y = i/3*128
      
      if i == SegmentBase.count
        x = i%3*260
        y = i/3*224
        @edits << GUI::Button.new(4+x, 4+y, "New")
      else
        @edits << GUI::Button.new(4+x, 4+y, "Edit")
        @labels << GUI::Label.new(4+x, 4+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:segment_name, i)}>")
        @segments << SegmentPreview.new(SegmentBase.get(i).grid, 4+x, 32+y, true)
      end
    end
    
    @up = GUI::Button.new(780, 0, "▲")
    @down = GUI::Button.new(780, 576, "▼")
    @max_scroll = (@segments.length+6)/5
    move_edits
  end
  
  def update
    @edits.each_index do |i|
      if @edits[i].value
        $segment_id = i
        GUI::System.Clear
        
        if i == @edits.length-1
          $segment = SegmentBase.new
        else
          $segment = SegmentBase.get(i)
        end
        return $state=SegmentEdit.new
      end
    end
    
    if @up.value or key_press(MsWheelUp)
      @@scroll -= 1 if @@scroll > 0
      move_edits
    end
    
    if @down.value or key_press(MsWheelDown)
      @@scroll += 1 if @@scroll < @max_scroll
      move_edits
    end
  end
  
  def draw
    @segments.each{|s| s.draw}
    
    h = 5/(@max_scroll+5).to_f * 552
    y = 24 + 552.0/(@max_scroll+5) * @@scroll
    draw_quad(780, y, Color::WHITE, 800, y, Color::WHITE, 800, y+h, Color::WHITE, 780, y+h, Color::WHITE, 0)
  end
  
  def move_edits
    @edits.each.with_index do |edit, i|
      edit.y = (i/3-@@scroll)*160+128
      edit.disabled = (![0,1,2].include?(edit.y/224))
    end
    @labels.each.with_index do |label, i|
      label.y = (i/3-@@scroll)*160+4
      label.disabled = (![0,1,2].include?(label.y/224))
    end
    @segments.each.with_index do |segment, i|
      segment.y = (i/3-@@scroll)*160+32
      segment.disabled = (![0,1,2].include?(segment.y/224))
    end
  end
end