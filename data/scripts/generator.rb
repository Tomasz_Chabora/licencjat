class Generator
  MAX_PASSES = 10
  
  attr_reader :state
  
  def initialize
    seed = Random.new_seed
    @seed = seed
    @state = 0 #start
    @pass = 1
    
    Thread::abort_on_exception = true
    Thread.new do
      create_world
    end
    
    $state = self
  end
  
  def create_world
    dt = milliseconds
    $world = @world = World.new(@seed)
    @state += 1 #locations
    @world.create_locations
    @state += 1 #areas
    @world.first_area
    @world.create_areas
    @state += 1 #rooms
    @world.first_room
    MAX_PASSES.times do
      @world.create_rooms
      @world.fill_empty_areas
      @pass += 1
      break if !@world.areas.find{|area| area.empty}
    end
    @state += 1 #fill
    @world.fill_room_stubs
    @state += 1 #segents
    @world.create_room_segments
    @state += 1 #contents
    @world.create_room_contents
    @world.create_savepoints
    @state += 1 #keys
    @world.create_keys
    @world.create_obstacles
    @state += 1 #items and enemies
    @world.create_items
    @world.create_room_enemies
    @state += 1 #finishing
    @world.count_squares_and_clean
    @state += 1 #done
    $world = nil
    puts "Stworzono #{@world.rooms.length} pomieszczeń"
    puts "Czas generacji: #{milliseconds - dt} ms"
  end
  
  def update
    return InGame.new(@world) if @state == 10
  end
  
  def draw
    text = ["Starting generator", "Creating locations","Creating areas", "Creating rooms (pass #{@pass})", "Filling up exits", "Placing room segments", "Creating room contents", "Generating keys and obstacles", "Making items and enemies", "Finishing generation", "Done"][@state]
    fnt("Courier New", Room::TILE).draw(text + '.' * ($time/8%4), Room::TILE, SCREEN_HEIGHT - Room::TILE*2, 0)
  end
  
  def self.pick_random(set, base)
    if set.find{|s| mod = base.call(s).has_modifier?("prefer") and Generator.condition_met(mod)}
      set.select!{|s| mod = base.call(s).has_modifier?("prefer") and Generator.condition_met(mod)}
    else
      set.delete_if{|s| mod = base.call(s).has_modifier?("prefer")}
    end
    
    if set.find{|s| base.call(s).has_modifier?("priority")}
      set.select!{|s| base.call(s).has_modifier?("priority")}
    end
    
    if $segment_cache
      obstacles = set.select{|s| base.call(s).has_modifier?("require")}.collect{|s| base.call(s).has_modifier?("require").split[1]}
      
      if !obstacles.empty?
        x = $segment_cache[0].x + $segment_cache[1]
        y = $segment_cache[0].y + $segment_cache[2]
        $world.obstacles[[x, y]] ||= []
        $world.obstacles[[x, y]].concat(obstacles).uniq!
      end
    end
    
    if $prefer_obstacle
      set2 = set.select{|s| base.call(s).has_modifier?("require")}.reject{|s| mod = base.call(s).has_modifier?("require") and $prefer_obstacle[:banned].include?(mod.split[1])}
      set3 = set2.select{|s| mod = base.call(s).has_modifier?("require") and mod.split[1] == $prefer_obstacle[:preferred]}
      set = (set3.empty? ? set2.empty? ? set.reject{|s| base.call(s).has_modifier?("require")} : set2 : set3)
    else
      set2 = set.reject{|s| base.call(s).has_modifier?("require")}
      set = set2 if !set2.empty?
    end
    
    chances = {}
    set.each{|s| chances[s] = 
      (!$override_never && base.call(s).has_modifier?("never")) ? 0 : 
      base.call(s).has_modifier?("rare") ? 10 : 
      base.call(s).has_modifier?("uncommon") ? 100 : 
      base.call(s).has_modifier?("common") ? 700 : 
      500}
    pick_random_with_chances(chances)
  end
  
  def self.pick_random_with_chances(chances)
    sum = chances.values.inject{|sum, p| sum + p}
    chances.keys.each_cons(2) {|c| chances[c.last] = chances[c.first] + chances[c.last]}
    value = $world.rand(sum)
    chances.key(chances.values.find{|p| p >= value})
  end
  
  def self.condition_met(mod)
    mod = mod.split
    mod.shift(mod.index("if")+1)
    conditions = []
    
    current_condition = []
    mod.each do |word|
      current_condition << word
      if word == "or" or word == "and"
        current_condition.pop if word == "and"
        conditions << current_condition.join(' ')
        current_condition.clear
        conditions << :or if word == "or"
      end
    end
    conditions << current_condition.join(' ')
    
    conditions.each_index do |i|
      cond1 = conditions[i]
      cond2 = (conditions[i+1] || :or)
      
      if !eval_condition(cond1) || !eval_condition(cond2)
        return false
      end
    end
    
    true
  end
  
  def self.eval_condition(condition)
    return if !$segment_cache
    case condition
      when :or
      true
      when "up-left exit"
      segment = $segment_cache[0].get_segment($segment_cache[1], $segment_cache[2]-1)
      return if !segment
      $segment_cache[2] > 0 and segment.connections[L] != -1 and segment.base.get_connections(L)[segment.connections[L]].type != C_ROOM
      when "up-right exit"
      segment = $segment_cache[0].get_segment($segment_cache[1], $segment_cache[2]-1)
      return if !segment
      $segment_cache[2] > 0 and segment.connections[R] != -1 and segment.base.get_connections(R)[segment.connections[R]].type != C_ROOM
      when "no up", "no right", "no down", "no left"
      dir = {"no up" => U, "no right" => R, "no down" => D, "no left" => L}[condition]
      !$segment_cache[3][dir]
      when "far right"
      $segment_cache[1] == $segment_cache[0].w-1
      when "far left"
      $segment_cache[1] == 0
    end
  end
end

class World
  LOCATION_COUNT = 3
  PREFERRED_ROOM_COUNT = 100
  
  AREA_COUNT = 20
  PRIVILEGE = 10
  PRIVILEGE_COUNT = 1
  
  EXIT_TRIES = 50
  EXIT_CACHE = 20
  EXIT_THRESHOLD = 3
  BLOCKER_THRESHOLD = 20
  FILL_TRIES = 100
  
  OBSTACLE_CHANCE = 1000
  MAX_OBSTACLES = 5
  OBSTACLE_TRIES = 100
  KEY_DISTANCE = 10
  MIN_KEY_DISTANCE = 5
  KEY_TRIES = 100
  
  SAVE_MIN_DISTANCE = 10
  SAVE_DISTANCE = 15
  NO_SAVE_CHANCE = 2
  
  def create_locations
    locations = Range.new(0, LocationBase.count, true).to_a
    
    LOCATION_COUNT.times do
      id = locations[rand(locations.length)]
      locations.delete id
      
      @locations << Location.new(id)
    end
  end
  
  def first_area
    area = @locations.first.create_area
    area.x -= area.w/2
    area.y -= area.h/2
    @areas << area
  end
  
  def first_room
    @rooms << Room.new(0, @areas.first)
    exits = @rooms.last.make_exits
    @exits.concat exits
    
    EXIT_TRIES.times do
      if @exits.empty?
        exits = @rooms.last.make_exits
        @exits.concat(exits) if exits
      else
        break
      end
    end
  end
  
  def create_areas
    FILL_TRIES.times do |i|
      origin = @areas[rand(@areas.length)]
      available = origin.base.connects.reject{|av| origin.previous and av[:id] == origin.previous.type}
      available = origin.base.connects if available.empty?
      
      chances = {}
      available.each{|c| chances[c] = (AreaBase.has_con_mod?(c, "uncommon") ? 200 : 500)}
      connect = Generator.pick_random_with_chances(chances)
      area = Area.new(connect[:id])
      area.location = get_location(area)
      area.previous = origin
      
      case connect[:type]
        when U
        area.x = origin.x - area.w + 1 + rand(area.w + origin.w - 1)
        area.y = origin.y - area.h
        
        when R
        area.x = origin.x + origin.w
        area.y = origin.y - area.h + 1 + rand(area.h + origin.h - 1)
        
        when D
        area.x = origin.x - area.w + 1 + rand(area.w + origin.w - 1)
        area.y = origin.y + origin.h
        
        when L
        area.x = origin.x - area.w
        area.y = origin.y - area.h + 1 + rand(area.h + origin.h - 1)
      end
      
      next if !validate_area(area, connect[:type]) or area_intersect(area)
      @areas << area
      
      if mod = area.base.has_modifier?("set mark") and !@area_marks[mod.split.last]
        @area_marks[mod.split.last] = area
      end
      
      break if @areas.length == AREA_COUNT
    end
  end
  
  def create_rooms
    FILL_TRIES.times do |i|
      placed = false
      room = nil
      @exits.shuffle(random: @random).each do |_exit|
        @prev_exits.push _exit
        @prev_exits.shift if @prev_exits.length > EXIT_CACHE
        $override_never = nil
        $override_never = true if @prev_exits.length == EXIT_CACHE and @prev_exits.uniq.length < EXIT_THRESHOLD
        
        exit, room2 = _exit
        x = room2.x + [exit.position, room2.w, exit.position, -1][exit.direction]
        y = room2.y + [-1, exit.position, room2.h, exit.position][exit.direction]
        
        if get_room(x, y)
          @exits.delete(_exit)
          next
        end
        
        area = get_area(x, y)
        room = area.create_room
        
        redo if room2.base.has_modifier?("skip same") and room.type == room2.type
        
        if try_place(room, room2, exit)
          room.origin = placed = _exit
          break
        end
      end
      
      if placed and exits = room.make_exits
        @rooms << room
        @exits.concat exits
        @exits.delete(placed)
        @exit_blocker = 0
      elsif @exits.empty?
        @exit_blocker += 1
        if @exit_blocker == BLOCKER_THRESHOLD
          @exit_blocker = 0
          
          blockers = @rooms.select{|room| room.exits.length == 1 and room.area.privilege}
          blockers.delete_if{|b| get_room(b.x, b.y-1) || !get_area(b.x, b.y-1) and get_room(b.x+1, b.y) || !get_area(b.x+1, b.y) and get_room(b.x, b.y+1) || !get_area(b.x, b.y+1) and get_room(b.x-1, b.y) || !get_area(b.x-1, b.y)}
          next if blockers.empty?
          
          blocker = blockers[rand(blockers.length)]
          next if !blocker or !blocker.origin
          @exits << blocker.origin
          @rooms.delete blocker
          next
        end
      elsif @prev_exits.length == EXIT_CACHE and @prev_exits.uniq.length == 1
        exit, room = @prev_exits.last
        @exits.delete(@prev_exits.last)
        room.exits.delete exit
        @prev_exits.clear
        room.restrict_exits = true
      end
      
      if @areas.find{|area| area.empty} and @exits.empty?
        areas_hash = areas.collect{|area| [area, 0]}.to_h
        @rooms.each{|room| areas_hash[room.area] += 1}
        room_chances = @rooms.reject{|r| r.base.has_modifier?("never")}.collect{|room| [room, 2**([@rooms.length - areas_hash[room.area] + (room.area.privilege ? PRIVILEGE : 1) - room.exits.length, 0].max)]}.to_h
        
        EXIT_TRIES.times do
          if @areas.find{|area| area.empty} and @exits.empty?
            room = Generator.pick_random_with_chances(room_chances)
            exits = room.make_exits
            @exits.concat(exits) if exits
          else
            break
          end
        end
      end
    end
  end
  
  def fill_empty_areas
    FILL_TRIES.times do
      empty_areas = @areas.select{|area| area.empty}
      return if empty_areas.empty?
      can_neighbour = false
      
      empty_areas.shuffle(random: @random).each do |area|
        neighbours = identify_area_neighbours(area)
        if neighbours.empty?
          neigbours = identify_area_neighbour_areas(area)
          next if neigbours.empty?
          neigbours.each{|neighbours| neighbours.privilege = PRIVILEGE_COUNT}
          break
        end
        can_neighbour = true
        
        room2 = neighbours[i = rand(neighbours.length)].first
        dir = neighbours[i].last
        
        hor = rand([room2.w, area.x + area.w - room2.x].min ,[0, area.x - room2.x].max)
        ver = rand([room2.h, area.y + area.h - room2.y].min ,[0, area.y - room2.y].max)
        exit = room2.force_exit(dir, {U => hor, R => ver, D => hor, L => ver}[dir])
        next if !exit
        
        room = area.create_room
        if try_place(room, room2, exit) and exits = room.make_exits
          room.origin = [exit, room2]
          @rooms << room
          @exits.concat exits
        else
          @exits << [exit, room2]
        end
      end
      
      return if !can_neighbour
    end
  end
  
  def fill_room_stubs
    FILL_TRIES.times {if !@exits.empty?
      placed = false
      room = nil
      @exits.shuffle(random: @random).each do |_exit|
        exit, room2 = _exit
        x = room2.x + [exit.position, room2.w, exit.position, -1][exit.direction]
        y = room2.y + [-1, exit.position, room2.h, exit.position][exit.direction]
        
        if get_room(x, y)
          @exits.delete(_exit)
          break
        end
        
        $override_never = true
        area = get_area(x, y)
        room = area.create_room
        $override_never = nil
        
        if try_place(room, room2, exit)
          room.origin = placed = _exit
          break
        end
      end
      
      if placed and exits = room.make_exits
        @rooms << room
        @exits.concat(exits)
      else
        next
      end
      
      @exits.delete(placed)
    else
      break
    end}
    
    if !@exits.empty?
      @exits.each do |exit|
        exit.last.exits.delete(exit.first)
      end
    end
  end
  
  def create_room_segments
    @rooms.each{|room| room.make_segments}
  end
  
  def create_room_contents
    @rooms.each{|room| room.make_contents}
  end
  
  def create_room_enemies
    @rooms.each{|room| room.make_contents(true)}
  end
  
  def create_keys
    useful_skills = @obstacles.values.flatten.uniq.collect{|obs| Relic::SHORT_NAMES.key(obs)}
    
    KEY_TRIES.times do
      @items.shuffle(random: @random).each do |spot|
        available = useful_skills - @keys.collect{|key| key[:item].type if key[:item].class == Relic}
        return if available.empty?
        
        room = spot.first
        item = spot.last
        
        x = room.x + item.x/SegmentBase::WIDTH
        y = room.y + item.y/SegmentBase::HEIGHT
        
        if map_distance(last_key[:x], last_key[:y], x, y) > KEY_DISTANCE and !@keys.find{|key| map_distance(key[:x], key[:y], x, y) < MIN_KEY_DISTANCE}
          item.data[:item] = Relic.new(available[rand(available.length)])
          
          next_x, next_y = room.x + item.x/SegmentBase::WIDTH, room.y + item.y/SegmentBase::HEIGHT
          @paths.concat Path.create_paths(last_key[:x], last_key[:y], next_x, next_y)
          @keys << {x: next_x, y: next_y, item: item.data[:item]}
          @items.delete(spot)
        end
      end
    end
  end
  
  def create_obstacles
    keys = []
    @paths.each do |path| next if path == @paths.first
      key = @keys.find{|k| k[:x] == path.start.x and k[:y] == path.start.y}
      if key
        keys << Relic::SHORT_NAMES[key[:item].type]
        path.keys.replace(keys)
      else
        puts "Błąd generacji: utworzono ścieżkę bez klucza, z [#{path.start.x}, #{path.start.y}] do [#{path.end.x}, #{path.end.y}]"
        next
      end
      
      key = @keys.find{|k| k[:x] == path.end.x and k[:y] == path.end.y}
      if key
        path.to_key = Relic::SHORT_NAMES[key[:item].type]
      end
    end
    
    update_accessibility
    
    @paths.each do |path| next if path == @paths.first
      obstacles_placed = 0
      OBSTACLE_TRIES.times do
        av_obstacles = @obstacles.each.collect{|pos, obs| pos if !(keys & obs).empty?}.compact.select{|obs| path.on_path?(*obs)}
        
        $prefer_obstacle = {preferred: path.keys.last, banned: []}
        break if av_obstacles.empty?
        
        obstacle = av_obstacles[rand(av_obstacles.length)]
        @obstacles.delete_if{|pos, obs| pos == obstacle}
        place_obstacle(obstacle)
        break if obstacles_placed == MAX_OBSTACLES
      end
    end
    
    obstacles_placed = 0
    
    $prefer_obstacle[:preferred] = nil if $prefer_obstacle
    OBSTACLE_TRIES.times do
      av_obstacles = @obstacles.each.collect{|pos, obs| pos if !(keys & obs).empty?}.compact
      
      break if av_obstacles.empty?
      
      obstacle = av_obstacles[rand(av_obstacles.length)]
        @obstacles.delete_if{|pos, obs| pos == obstacle}
      place_obstacle(obstacle)
      obstacles_placed += 1
      break if obstacles_placed == MAX_OBSTACLES
    end
    
    $prefer_obstacle = nil
  end
  
  def place_obstacle(obstacle)
    banned = {U => [], R => [], D => [], L => [], :none => []}
    banned[:none] << "dash"
    
    @paths.each do |path| next if not node = path.on_path?(*obstacle)
      cant = Relic::SHORT_NAMES.values - path.keys
      banned[:none].concat cant
      dir = path.node_direction(*obstacle)
      banned[dir].concat cant
      banned[OPPOSITE[dir]].concat(cant) if dir != :none
      if node.previous.first
        dir2 = path.node_direction(node.previous.first.x, node.previous.first.y)
        banned[OPPOSITE[dir2]].concat(cant) if dir2 != :none
      end
    end
    
    room = get_room(*obstacle)
    old_segment = room.get_segment(obstacle[0] - room.x, obstacle[1] - room.y)
    up = old_segment.get_pattern(U)
    right = old_segment.get_pattern(R)
    down = old_segment.get_pattern(D)
    left = old_segment.get_pattern(L)
    
    segments = room.base.segments.select do |s| segment = SegmentBase.get(s)
      can_be = true
      has_obstacle_connection = false
      
      next if obstacle[1] == room.y && segment.has_modifier?("no top") or obstacle[1] != room.y + room.h-1 && !segment.has_modifier?("bottom only") or segment.has_modifier?("no dead end") && old_segment.connections.count(-1) == 3
      
      [U, R, D, L].each do |dir|
        next if old_segment.connections[dir] == -1
        can_be2 = false
        segment.get_connections(dir).each do |c|
          if c.get_pattern(dir) == {U => up, R => right, D => down, L => left}[dir]
            can_be2 = true
            has_obstacle_connection = true if (mod = c.has_modifier?("require") and !banned[dir].include?(mod.split[1]) and !mod.split.include?("unsafe") || !is_back?(obstacle[0], obstacle[1], dir, []))
          end
        end
        can_be = (can_be and can_be2)
        
        break if !can_be
      end
      
      can_be and has_obstacle_connection || (mod = segment.has_modifier?("require") and !banned[:none].include?(mod.split[1]) and !mod.split.include?("unsafe") || mod.split.include?("down") && !is_back?(obstacle[0], obstacle[1], U, []))
    end
    
    return if segments.empty?
    
    $prefer_obstacle[:banned].replace(banned[:none])
    segment = Segment.new(segments[rand(segments.length)])
    $prefer_obstacle[:banned].replace(banned[U])
    segment.set_connection(:force, U, up, nil)
    $prefer_obstacle[:banned].replace(banned[R])
    segment.set_connection(:force, R, right, nil)
    $prefer_obstacle[:banned].replace(banned[D])
    segment.set_connection(:force, D, down, nil)
    $prefer_obstacle[:banned].replace(banned[L])
    segment.set_connection(:force, L, left, nil)
    room.replace_segment(obstacle[0] - room.x, obstacle[1] - room.y, segment)
    
  end
  
  def create_savepoints
    @save_points << room = get_room(0, 0)
    room.save_point = true
    
    saverooms = @rooms.select{|r| r.w + r.h == 2 and r.placeholders.find{|plc| plc.type == :save}}
    placed_save = true
    
    distance_map = {}
    
    while placed_save
      placed_save = false
      update_save_distances(distance_map)
      
      saverooms.shuffle(random: @random).each do |room|
        if !@save_points.find{|save| distance_map[[room.x, room.y]] < SAVE_DISTANCE}
          next if rand(NO_SAVE_CHANCE) != 0 and @save_points.find{|save| distance_map[[room.x, room.y]] < SAVE_MIN_DISTANCE}
          @save_points.push(saverooms.delete(room))
          room.save_point = true
          room.placeholders.delete_if{|plc| plc.type != :save}
          placed_save = true
          break
        end
      end
    end
    
    @items.delete_if{|item| item.first.save_point}
  end
  
  def create_items
    @items.each do |item|
      if rand(10) == 0
        item.last.data[:item] = Booster.new(0)
      else
        item.last.data[:item] = Item.new
      end
    end
  end
  
  def try_place(room, room2, exit)
    can_connect = false
    room.base.segments.each do |s|
      SegmentBase.get(s).get_connections(OPPOSITE[exit.direction]).each{|c| if room2.base.segments.find{|s2| SegmentBase.get(s2).get_connections(exit.direction).find{|c2| c2.get_pattern(exit.direction) == c.get_pattern(OPPOSITE[exit.direction])}}
        can_connect = true 
        break
      end}
      break if can_connect
    end
    return if !can_connect
    
    room.x = room2.x + [exit.position, room2.w, exit.position, -room.w][exit.direction]
    room.y = room2.y + [-room.h, exit.position, room2.h, exit.position][exit.direction]
    room.x -= $world.rand(room.w) if [U, D].include?(exit.direction)
    room.y -= $world.rand(room.h) if [R, L].include?(exit.direction)
    
    validate_room(room)
  end
end

class Location
  def create_area
    available = base.areas.reject{|area| AreaBase.get(area).has_modifier?("no start")}
    area = Area.new(available[$world.rand(available.length)])
    area.location = self
    area
  end
  
  def generate_name
    name = base.names[$world.rand(base.names.length)].dup
    
    base.replacements.each do |rep|
      name.gsub!(rep[:key], Generator.generate_name(rep[:strings][$world.rand(rep[:strings].length)]))
    end
    
    name
  end
end

class Area
  def initialize(type)
    @x = 0
    @y = 0
    @type = type
    
    @w = base.min_width + $world.rand(base.max_width - base.min_width + 1)
    @h = base.min_height + $world.rand(base.max_height - base.min_height + 1)
    tilesets = base.tilesets.select{|tileset| tileset.split.length == 1}
    @tileset = tilesets[$world.rand(tilesets.length)]
    
    @empty = true
  end
  
  def create_room
    remove_instance_variable(:@privilege) if @privilege and (@privilege -= 1) == 0
    remove_instance_variable(:@empty) if @empty
    room = Room.new(Generator.pick_random(base.rooms, lambda{|i| RoomBase.get(i)}), self)
    
    if room.base.has_modifier?("stretch hor")
      room.w = [@w, room.base.max_width].min
    end
    room
  end
end

class Room
  def initialize(type, area)
    @x = 0
    @y = 0
    @type, @area = type, area
    @segments = {}
    @exits = []
    @placeholders = []
    
    @w = base.min_width + $world.rand(base.max_width - base.min_width + 1)
    @h = base.min_height + $world.rand(base.max_height - base.min_height + 1)
    @unique_id = $world.rand(4294967296)
  end
  
  def make_exits(args = {})
    return if @restrict_exits
    exits = []
    
    @w.times do |x|
      room = $world.get_room(@x + x, @y - 1)
      if room and room.get_exit(D, @x - room.x + x)
        return if base.has_modifier?("restrict hor") or base.has_modifier?("no up absolute")
        add_exit(U, x, nil)
      elsif !room and !base.has_modifier?("no up") and area = $world.get_area(@x + x, @y - 1) and area.base.rooms.find{|r| !RoomBase.get(r).has_modifier?("restrict hor")} and $world.rand(1000) < base.horizontal_exit/2 * (base.horizontal_exit < 100 && @area.privilege ? 10 : 1)
        add_exit(U, x, exits)
      end
      
      room = $world.get_room(@x + x, @y + @h)
      if room and room.get_exit(U, @x - room.x + x)
        return if base.has_modifier?("restrict hor") or base.has_modifier?("no down absolute")
        add_exit(D, x, nil)
      elsif !room and !base.has_modifier?("no down") and area = $world.get_area(@x + x, @y + @h) and area.base.rooms.find{|r| !RoomBase.get(r).has_modifier?("restrict hor")} and $world.rand(1000) < base.horizontal_exit/2 * (base.horizontal_exit < 100 && @area.privilege ? 10 : 1)
        add_exit(D, x, exits)
      end
    end
    
    @h.times do |y|
      room = $world.get_room(@x + @w, @y + y)
      if room and room.get_exit(L, @y - room.y + y)
        return if base.has_modifier?("restrict ver")
        add_exit(R, y, nil)
      elsif !room and !base.has_modifier?("no right") and $world.get_area(@x + @w, @y + y) and $world.rand(1000) < base.vertical_exit/2 * (base.vertical_exit < 100 && @area.privilege ? 10 : 1)
        add_exit(R, y, exits)
      end
      
      room = $world.get_room(@x - 1, @y + y)
      if room and room.get_exit(R, @y - room.y + y)
        return if base.has_modifier?("restrict ver")
        add_exit(L, y, nil)
      elsif !room and !base.has_modifier?("no left") and $world.get_area(@x - 1, @y + y) and $world.rand(1000) < base.vertical_exit/2 * (base.vertical_exit < 100 && @area.privilege ? 10 : 1)
        add_exit(L, y, exits)
      end
    end
    
    exits
  end
  
  def force_exit(direction, position)
    return if direction == D && base.has_modifier?("no down absolute") or direction == U && base.has_modifier?("no up absolute") or [U, D].include?(direction) && base.has_modifier?("restrict hor absolute")
    add_exit(direction, position, nil)
  end
  
  def make_segments
    @w.times do |x|
      @h.times do |y|
        is_border = {U => y==0, R => x==@w-1, D => y==@h-1, L => x==0}
        up = get_pattern(x, y, U) || (is_border[U] && get_exit(U, x) && true)
        right = get_pattern(x, y, R) || (is_border[R] && get_exit(R, y) && true)
        down = get_pattern(x, y, D) || (is_border[D] && get_exit(D, x) && true)
        left = get_pattern(x, y, L) || (is_border[L] && get_exit(L, y) && true)
        
        segments = get_matching_segments(up, right, down, left, is_border)
        next puts "Brak pasujących segmentów w [#{@x+x}, #{@y+y}]" if segments.empty?
        
        $segment_cache = [self, x, y, {U => (up || !is_border[U]), R => (right || !is_border[R]), D => (down || !is_border[D]), L => (left || !is_border[L])}]
        segment = Segment.new(Generator.pick_random(segments, lambda{|i| SegmentBase.get(i)}))
        segment.set_connection(self, U, up, is_border[U] ? (get_exit(U, x) || true) : nil)
        segment.set_connection(self, R, right, is_border[R] ? (get_exit(R, y) || true) : nil)
        segment.set_connection(self, D, down, is_border[D] ? (get_exit(D, x) || true) : nil)
        segment.set_connection(self, L, left, is_border[L] ? (get_exit(L, y) || true) : nil)
        $segment_cache = nil
        
        is_border.each_pair do |direction, border|
          if !border or not room = $world.get_room({U => @x+x, R => @x+@w, D => @x+x, L => @x-1}[direction], {U => @y-1, R => @y+y, D => @y+@h, L => @y+y}[direction])
            next
          end
          if exit = room.get_exit(OPPOSITE[direction],
              {U => @x - room.x + x, R => @y - room.y + y, D => @x - room.x + x, L => @y - room.y + y}[direction])
            exit.pattern = segment.get_pattern(direction)
          end
        end
        
        @segments[[x, y]] = segment
        
        if mod = segment.base.has_modifier?("require")
          $world.obstacles << [self, x, y, mod.split[1]]
        end
      end
    end
  end
  
  def make_contents(enemy_phase = false)
    @w.times do |x|
      @h.times do |y|
        next if !@segments[[x,y]]
        @segments[[x,y]].base.layers.each do |layer|
          next if (layer.type == SegmentLayer::T_ENEMY) != enemy_phase
          process_content_layer(x * SegmentBase::WIDTH, y * SegmentBase::HEIGHT, layer, @segments[[x,y]])
        end
      end
    end
  end
  
  def process_content_layer(x1, y1, layer, segment)
    grid = segment.get_layer_grid(layer)
    
    helpers = {}
    placeholders = []
    
    if layer.has_modifier?("single")
      helpers[:single] ||= []
    end
    
    if layer.has_modifier?("up to")
      helpers[:up_to] ||= []
    end
    
    grid.each.with_index do |v, t| x2, y2 = t % SegmentBase::WIDTH, t / SegmentBase::WIDTH
      next if !v
      
      case layer.type
        when SegmentLayer::T_TREASURE
        return if layer.has_modifier?("dead end") and segment.connections.count(-1) < 3
        next if layer.has_modifier?("opposite") and [y2 < 7, x2 > 10, y2 > 7, x2 < 10][segment.connections.index{|i| i != -1}]
        if read_grid_tile(x1+x2, y1+y2+1) == :wall
          item = Placeholder.new(x1+x2, y1+y2, :item, item: nil)
          placeholders << item
          helpers[:single] << item if helpers[:single]
        end
        
        when SegmentLayer::T_ENEMY
        if !helpers[:space] and mod = layer.has_modifier?("space")
          helpers[:space] = mod.split.last.to_i
        elsif helpers[:space]
          helpers[:space] -= 1
          next (helpers[:space] = nil if helpers[:space] == 0)
        end
        
        enemies = @area.base.enemies.collect{|enemy| enemy.split.first.to_sym}
        enemies.delete_if do |enemy|
          Enemy.has_modifier?(enemy, "on ground") && read_grid_tile(x1+x2, y1+y2+1) == :air or
          Enemy.has_modifier?(enemy, "on ceiling") && read_grid_tile(x1+x2, y1+y2-1) == :air
        end
        
        next if enemies.empty?
        enemy = Placeholder.new(x1+x2, y1+y2, :enemy, type: enemies[$world.rand(enemies.length)])
        placeholders << enemy
        helpers[:up_to] << enemy if helpers[:up_to]
        
        when SegmentLayer::T_SAVE
        placeholders << Placeholder.new(x1+x2, y1+y2, :save) if read_grid_tile(x1+x2, y1+y2+1) == :wall
      end
    end
    
    if helpers[:single]
      (helpers[:single].length-1).times do
        placeholders.delete helpers[:single].delete_at($world.rand(helpers[:single].length))
      end
    end
    
    if helpers[:up_to]
      (helpers[:up_to].length - $world.rand(layer.has_modifier?("up to").split.last.to_i+1)).times do
        placeholders.delete(helpers[:up_to].delete_at($world.rand(helpers[:up_to].length)))
      end
    end
    
    @placeholders.concat placeholders
    $world.items.concat placeholders.select{|p| p.type == :item}.collect{|plc| [self, plc]}
  end
end

class Segment
  def set_connection(room, direction, pattern, exit)
    return if exit == true or room == :force && !pattern
    connections = filter_connections(room, direction, pattern, exit)
    connections = filter_connections(room, direction, pattern, exit, allow_passive: true) if connections.empty?
    
    if connections.empty?
      return
      dir = {U => "U", R => "R", D => "D", L => "L"}[direction]
      raise "Brak połączenia segmentu #{@id} w kierunku #{dir}" if !pattern or pattern == true
      raise "Brak połączenia segmentu #{@id} w kierunku #{dir} dla wzorca [#{pattern.join(" ")}]"
    end
    
    @connections[direction] = Generator.pick_random(connections, lambda{|i| base.get_connections(direction)[i]})
    
    if exit and room != :force
      room2 = case direction
        when U
        $world.get_room(room.x, room.y-1)
        when R
        $world.get_room(room.x+room.w, room.y)
        when D
        $world.get_room(room.x, room.y+room.h)
        when L
        $world.get_room(room.x-1, room.y)
      end
      return if not room2
    
      diff = {U => room.x - room2.x, R => room.y - room2.y, D => room.x - room2.x, L => room.y - room2.y}
      exit2 = room2.get_exit(OPPOSITE[direction], exit.position + diff[direction])
      return if not exit2
      exit2.pattern = get_pattern(direction)
    end
  end
  
  def filter_connections(room, direction, pattern, exit, args = {})
    base.get_connections(direction).map.with_index do |c, i|
      i if (!pattern || pattern == true and !c.has_modifier?("passive")) || c.get_pattern(direction) == pattern || args[:allow_passive] and
        c.type == C_BOTH || !exit == (c.type == C_ROOM) || room == :force and
        !exit || !(room2 = $world.get_room(room.x + {U => exit.position, R => room.w, D => exit.position, L => -1}[direction], room.y + {U => -1, R => exit.position, D => room.h, L => exit.position}[direction])) || room2.base.segments.find{|s| SegmentBase.get(s).get_connections(OPPOSITE[direction]).find{|c2| c2.get_pattern(OPPOSITE[direction]) == c.get_pattern(direction)}}
      end.compact
  end
end

class Placeholder
  attr_reader :x ,:y, :type, :data
  
  def initialize(x, y, type, data = {})
    @x, @y, @type, @data = x, y, type, data
  end
end

class PathNode
  attr_reader :x, :y, :previous
  
  def initialize(x, y)
    @x, @y = x, y
    @previous = []
  end
end

class Path
  attr_accessor :to_key
  attr_reader :keys
  
  def self.create_paths(x0, y0, xk, yk)
    finish = PathNode.new(xk, yk)
    
    visited = [finish]
    unvisited = [PathNode.new(x0, y0)]
    
    while !unvisited.empty?
      current = unvisited.pop
      visited << current
      
      [U, R, D, L].each do |dir|
        x, y = current.x + {U => 0, R => 1, D => 0, L => -1}[dir], current.y + {U => -1, R => 0, D => 1, L => 0}[dir]
        next if !nxt_room = $world.get_room(x, y) or !current.previous.empty? && current.previous.find{|prev| prev.x == x && prev.y == y }
        cur_room = $world.get_room(current.x, current.y)
        next if not cur_room == nxt_room || cur_room.get_exit(dir, {U => x-cur_room.x, R => y-cur_room.y, D => x-cur_room.x, L => y-cur_room.y}[dir])
        
        if node = visited.find{|nd| nd.x == x and nd.y == y}
          node.previous << current
        else
          nxt = PathNode.new(x, y)
          nxt.previous << current
          unvisited << nxt
        end
      end
    end
    
    paths = []
    create_branching_path([finish], paths, [x0, y0])
    paths.each{|p| p.clean}
    paths
  end
  
  def self.create_branching_path(nodes, paths, terminate)
    if nodes.uniq == nodes
      paths << path = Path.new(nodes)
    else
      return
    end
    
    while nodes.last.x != terminate[0] or nodes.last.y != terminate[1]
      if nodes.last.previous.length > 1
        nodes.last.previous.last(nodes.last.previous.length-1).each do |prev|
          create_branching_path(nodes.dup.push(prev), paths, terminate)
        end
      end
      
      nodes << nodes.last.previous.first
      path.append_node(nodes.last)
    end
  end

  
  def initialize(nodes)
    @nodes = nodes
    @keys = []
  end
  
  def on_path?(x, y)
    @nodes.find{|p| p.x == x and p.y == y}
  end
  
  def node_direction(x, y)
    node = @nodes.reverse.index{|p| p.x == x and p.y == y}
    node2 = get_node(node+1)
    if node2 and (node2.x <=> x) != 0
      return [nil, R, L][node2.x <=> x]
    elsif node2 and (node2.y <=> y) != 0
      return [nil, D, U][node2.y <=> y]
    end
    :none
  end
  
  def append_node(node)
    @nodes << node
  end
  
  def start
    @nodes.last
  end
  
  def get_node(i)
    @nodes.reverse[i]
  end
  
  def end
    @nodes.first
  end
  
  def clean
    @nodes.uniq!
  end
end

class Item < Entity
  def initialize(id = nil)
    @id = (id ? id : $world.rand(ItemBase.count))
    @name = generate_name
    @icon = generate_icon
  end
  
  def generate_name
    name = base.names[$world.rand(base.names.length)].dup
    
    base.replacements.each do |rep|
      name.gsub!(rep[:key], Generator.generate_name(rep[:strings][$world.rand(rep[:strings].length)]))
    end
    
    name
  end
  
  def generate_icon
    icon = []
    
    last_color = nil
    base.layers.each do |layer|
      new_layer = {:color => [16, 16, 16, 16]}
      graphic = layer[$world.rand(layer.length)].split
      
      if i = graphic.index("color")
        case graphic[i+1]
          when "random"
          if graphic[i+2] == "saturated"
            color = Color.from_hsv(i=$world.rand(360), 1, 1)
            last_color = new_layer[:color] = [16, color.red/16, color.green/16, color.blue/16]
          else
            last_color = new_layer[:color] = [16] + Array.new(3) {$world.rand(17)}
          end
          
          when "darker"
          last_color = new_layer[:color] = [16] + Array.new(3) {|i| [last_color[i+1] - 1 - $world.rand(4), 0].max}
        end
      end
      
      if graphic.include?("additive")
        new_layer[:mode] = :additive
      else
        new_layer[:mode] = :default
      end
      
      new_layer[:graphic] = graphic.first
      icon << new_layer
    end
    
    icon
  end
end