class AreaBase
  attr_accessor :modifiers
  attr_writer :min_width, :min_height, :max_width, :max_height, :rooms, :connects, :enemies, :tilesets
  
  def self.refresh
    @@areas = []
    (Dir.entries("data/base/Areas") - ['.','..']).each do |id|
      @@areas << Marshal.load(f = File.new("data/base/Areas/#{id}"))
      f.close
    end
  end
end

class AreaEdit
  def initialize
    @return = GUI::Button.new(720, 4, "Return")
    @save = GUI::Button.new(4, 4, "Save")
    GUI::Label.new(48, 4, "Area " + $area_id.to_s.rjust(4, "0"))
    @name = GUI::Textbox.new(192, 4, 32)
    @name.value = Metadata.load(:area_name, $area_id)
    
    GUI::Label.new(4, 224, "Rooms")
    @checks = []
    @labels = []
    y0 = 256
    RoomBase.count.times do |i|
      x = 4
      y = i*32
      
      @checks << GUI::Checkbox.new(x, y0+y)
      @checks.last.value = true if $area.rooms.include?(i)
      @labels << GUI::Label.new(48+x, y0+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:room_name, i)}> Segments: " + RoomBase.get(i).segments.join(", "))
    end
    
    GUI::Label.new(4, 40, "Min. width")
    @min_w = GUI::Spinner.new(4, 64, 1, 100)
    @min_w.value = $area.min_width
    
    GUI::Label.new(128, 40, "Max. width")
    @max_w = GUI::Spinner.new(128, 64, 1, 100)
    @max_w.value = $area.max_width
    
    GUI::Label.new(252, 40, "Min. height")
    @min_h = GUI::Spinner.new(252, 64, 1, 100)
    @min_h.value = $area.min_height
    
    GUI::Label.new(376, 40, "Max. height")
    @max_h = GUI::Spinner.new(376, 64, 1, 100)
    @max_h.value = $area.max_height
    
    GUI::Label.new(8, 88, "Modifiers")
    @modifiers = GUI::Textbox.new(8, 120, 40)
    @modifiers.value = $area.modifiers.join("; ")
    @mods = @modifiers.value
    
    GUI::Label.new(8, 144, "Enemies (+modifiers)")
    @enemies = GUI::Textbox.new(8, 168, 40)
    @enemies.value = $area.enemies.join("; ")
    
    GUI::Label.new(8, 192, "Tilesets (+modifiers)")
    @tilesets = GUI::Textbox.new(8, 216, 48)
    @tilesets.value = $area.tilesets.join("; ")
    
    @connects = []
    GUI::Label.new(500, 40, "Connects")
    $area.connects.each.with_index do |con, i|
      @connects << new_connect_edit(i)
      @connects.last[0].value = con[:id]
      @connects.last[1].value = con[:type]
      @connects.last[2].data = con[:modifiers].join("; ")
    end
    @add_connect = GUI::Button.new(750, 64, "+")
    @delete_connect = GUI::Button.new(780, 64, "-")
    
    @up = GUI::Button.new(780, 256, "▲")
    @down = GUI::Button.new(780, 576, "▼")
    @scroll = 0
    @max_scroll = [@checks.length-11, 0].max
    move_rooms
  end
  
  def new_connect_edit(i)
    [GUI::Dropdown.new(450, 64 + i*24, *(0...AreaBase.count).to_a.collect{|i| Metadata.load(:area_name, i)[0..10]}),
        GUI::Dropdown.new(610, 64 + i*24, "U", "R", "D", "L"),
        GUI::Button.new(690, 64 + i*24, "MOD")]
  end
  
  def update
    if @return.value
      GUI::System.Clear
      AreaBase.refresh
      
      return $state = AreaBrowser.new
    end
    
    if @add_connect.value
      @connects << new_connect_edit($area.connects.length)
      $area.connects << {:id => 0, :type => 0}
    end
    
    if @delete_connect.value and !@connects.empty?
      @connects.last.each{|c| c.remove}
      @connects.pop
      $area.connects.pop
    end
    
    if c = @connects.find{|c| c.last.value}
      modedit(c)
    end
    
    if @up.value or key_press(MsWheelUp)
      @scroll -= 1 if @scroll > 0
      move_rooms
    end
    
    if @down.value or key_press(MsWheelDown)
      @scroll += 1 if @scroll < @max_scroll
      move_rooms
    end
    
    save if @save.value
  end
  
  def draw
    h = 12/(@max_scroll+12).to_f * 296
    y = 280 + 296.0/(@max_scroll+12) * @scroll
    draw_quad(780, y, Color::WHITE, 800, y, Color::WHITE, 800, y+h, Color::WHITE, 780, y+h, Color::WHITE, 0)
  end
  
  def save
    modedit(nil)
    
    Metadata.store(:area_name, $area_id, @name.value)
    $area.min_width = @min_w.value
    $area.min_height = @min_h.value
    $area.max_width = @max_w.value
    $area.max_height = @max_h.value
    
    $area.rooms = @checks.each_index.to_a.select{|v| @checks[v].value}
    $area.modifiers = @mods.split(/;\s*/)
    $area.enemies = @enemies.value.split(/;\s*/)
    $area.tilesets = @tilesets.value.split(/;\s*/)
    
    $area.connects.replace(@connects.collect{|con| {:id => con[0].value, :type => con[1].value, :modifiers => con[2].data.to_s.split(/;\s*/)}})
    
    f = File.new("data/base/Areas/" + $area_id.to_s.rjust(4, "0"), 'w')
    Marshal.dump($area, f)
    f.close
  end
  
  def move_rooms
    y0 = 256
    @checks.each.with_index{|c, i|
      c.y = y0 + (i-@scroll)*32
      c.disabled = !(0..11).include?(i-@scroll)
    }
    @labels.each.with_index{|l, i|
      l.y = y0 + (i-@scroll)*32
      l.disabled = !(0..11).include?(i-@scroll)
    }
  end
  
  def modedit(c)
    if c
      if c.last.selected
        c.last.selected = nil
      else
        @connects.each{|cc| cc.last.selected = nil}
        c.last.selected = true
      end
    else
      @connects.each{|cc| cc.last.selected = nil}
    end
    
    if @mod_edit
      @mod_edit.data = @modifiers.value
      
      if cc = @connects.find{|cc| cc.last.selected}
        @mod_edit = cc.last
        @modifiers.value = cc.last.data
      else
        @mod_edit = nil
        @modifiers.value = @mods
      end
    else
      @mods = @modifiers.value
      if cc = @connects.find{|cc| cc.last.selected}
        @mod_edit = cc.last
        @modifiers.value = cc.last.data
      end
    end
  end
end

class AreaBrowser
  def initialize
    @rooms = []
    @edits = []
    
    (AreaBase.count+1).times do |i|
      x = 4
      y = i*32
      
      if i == AreaBase.count
        y = i*32
        @edits << GUI::Button.new(4+x, 4+y, "New")
      else
        @edits << GUI::Button.new(x, 4+y, "Edit")
        GUI::Label.new(48+x, 4+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:area_name, i)}> Rooms: " + AreaBase.get(i).rooms.join(", "))
      end
    end
  end
  
  def update
    @edits.each_index do |i|
      if @edits[i].value
        $area_id = i
        GUI::System.Clear
        
        if i == @edits.length-1
          $area = AreaBase.new
        else
          $area = AreaBase.get(i)
        end
        return $state=AreaEdit.new
      end
    end
  end
  
  def draw
  end
end