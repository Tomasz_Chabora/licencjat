class BackgroundBase
  attr_writer :layers
  
  def self.refresh
    @@backgrounds = []
    (Dir.entries("data/base/Backgrounds") - ['.','..']).each do |id|
      @@backgrounds << Marshal.load(f = File.new("data/base/Backgrounds/#{id}"))
      f.close
    end
  end
end

class BackgroundLayer
  attr_writer :position, :scroll_x, :scroll_y
  attr_reader :images
end

class BackgroundEdit
  def initialize
    @return = GUI::Button.new(720, 4, "Return")
    @save = GUI::Button.new(4, 4, "Save")
    GUI::Label.new(48, 4, "Background " + $background_id.to_s.rjust(4, "0"))
    @name = GUI::Textbox.new(192, 4, 32)
    @name.value = Metadata.load(:background_name, $background_id)
    
    GUI::Label.new(8, 32, "Layers")
    @add_layer = GUI::Button.new(8, 64, "+")
    @delete_layer = GUI::Button.new(48, 64, "-")
    
    @layer_edits = []
    $background.layers.reverse.each.with_index do |layer, i|
      @layer_edits << new_layer_edit(i)
      @layer_edits.last[1].value = layer.image
      @layer_edits.last[2].value = layer.position
      @layer_edits.last[3].value = layer.scroll_x
      @layer_edits.last[4].value = layer.scroll_y
      @layer_edits.last[5].value = layer.loop_x
      @layer_edits.last[6].value = layer.loop_y
    end
  end
  
  def new_layer_edit(i)
    ary = [GUI::Button.new(48, 64 + i*24, '^'),
      GUI::Textbox.new(80, 64 + i*24, 32),
      GUI::Dropdown.new(380, 64 + i*24, "Bottom", "Top", "Fill"),
      GUI::Spinner.new(496, 64 + i*24, -1000, 1000),
      GUI::Spinner.new(592, 64 + i*24, -1000, 1000),
      GUI::Checkbox.new(692, 64 + i*24),
      GUI::Checkbox.new(724, 64 + i*24)]
    ary.first.disabled = true if i == 0
    ary
  end
  
  def update
    if @return.value
      GUI::System.Clear
      BackgroundBase.refresh
      
      return $state = BackgroundBrowser.new
    end
    
    if @add_layer.value
      @layer_edits << new_layer_edit(@layer_edits.length)
     end
      
    if @delete_layer.value and !@layer_edits.empty?
      @layer_edits.last.each{|e| e.remove}
      @layer_edits.pop
    end
    
    @layer_edits.each.with_index do |edit, i|
      if edit.first.value
        temp = @layer_edits[i-1].dup
        @layer_edits[i-1].replace(edit)
        edit.replace(temp)
      end
    end
    
    save if @save.value
  end
  
  def draw
  end
  
  def save
    Metadata.store(:background_name, $background_id, @name.value)
    $background.layers = @layer_edits.collect{|l| BackgroundLayer.new(*l[1...l.length].collect{|ll| ll.value})}.reverse
    
    f = File.new("data/base/Backgrounds/" + $background_id.to_s.rjust(4, "0"), 'w')
    Marshal.dump($background, f)
    f.close
  end
end

class BackgroundBrowser
  def initialize
    @rooms = []
    @edits = []
    
    (BackgroundBase.count+1).times do |i|
      x = 4
      y = i*32
      
      if i == BackgroundBase.count
        y = i*32
        @edits << GUI::Button.new(4+x, 4+y, "New")
      else
        @edits << GUI::Button.new(x, 4+y, "Edit")
        GUI::Label.new(48+x, 4+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:background_name, i)}>")
      end
    end
  end
  
  def update
    @edits.each_index do |i|
      if @edits[i].value
        $background_id = i
        GUI::System.Clear
        
        if i == @edits.length-1
          $background = BackgroundBase.new
        else
          $background = BackgroundBase.get(i)
        end
        return $state=BackgroundEdit.new
      end
    end
  end
  
  def draw
  end
end