class GenericEffect
  DEFAULT_Z = 3
  
  attr_writer :x, :y
  
  def initialize(image, args = {})
    @image = image
    @args = {z: DEFAULT_Z, scale: 1, color: Color::WHITE}.merge(args)
    
    if @args[:animation]
      @args = {animation_iterations: 0, frame: 0, ticks: 0}.merge(@args)
      @args[:sequence] ||= tls(*@image[0..2]).each_index.to_a if @image.class == Array
    end
    
    if @args[:rotate]
      @args[:angle] ||= 0
    end
    
    if @args[:angle]
      @args = {origin_x: 0.5, origin_y: 0.5}.merge(@args)
    end
    
    @image[3] ||= 0 if @image.class == Array
  end
  
  def update
    if @args[:animation]
      @args[:ticks] += 1
      if @args[:ticks] == @args[:animation]
        @args[:ticks] = 0
        @args[:frame] += 1
        
        if @args[:frame] == @args[:sequence].length
          @args[:frame] = 0
          @args[:animation_iterations] += 1
        end
        
        @image[3] = @args[:sequence][@args[:frame]] if @image.class == Array
      end
    end
    
    @args[:angle] += @args[:rotate] if @args[:rotate]
  end
  
  def draw(x, y)
    scale_x = @args[:scale] * (@args[:mirror_x] ? -1 : 1)
    scale_y = @args[:scale] * (@args[:mirror_y] ? -1 : 1)
    
    image = (@image.class == Array ? tls(*@image[0..2])[@image[3]] : img(@image))
    if @args[:angle]
      image.draw_rot(x + image.width/2, y + image.height/2, @args[:z], @args[:angle], @args[:origin_x], @args[:origin_y], scale_x, scale_y, @args[:color])
    else
      image.draw(x + (@args[:mirror_x] ? image.width : 0), y + (@args[:mirror_y] ? image.height : 0), @args[:z], scale_x, scale_y, @args[:color])
    end
  end
  
  def [](arg)
    @args[arg]
  end
end

class GenericRepeater < Entity
  def initialize(x, y, repeats, &instruction)
    @x, @y, @instruction, @repeats = x, y, instruction, repeats
    init
  end
  
  def update
    self.instance_eval(&@instruction)
    @repeats -= 1
    remove if @repeats == 0
  end
end

class Particle < Entity
  def initialize(x, y, image, args = {})
    @x, @y = x, y
    @sprite = GenericEffect.new(image, args)
    @args = {vx: 0, vy: 0}.merge(args)
    
    init
  end
  
  def update
    @sprite.update
    
    @x += @args[:vx]
    @y += @args[:vy]
    @args[:vy] += 1
    
    remove if @y > $state.screen_y + SCREEN_HEIGHT
  end
  
  def draw
    @sprite.draw(@x, @y)
  end
end

class Trail < Entity
  def initialize(x, y, image, args = {})
    @x, @y = x, y
    @args = {animation: 1, repeat: 1}.merge(args)
    @sprite = GenericEffect.new(image, args)
    
    init
  end
  
  def update
    @sprite.update
    
    remove if @sprite[:animation_iterations] == @args[:repeat]
  end
  
  def draw
    @sprite.draw(@x, @y)
  end
end