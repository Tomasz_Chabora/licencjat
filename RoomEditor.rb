require "gosu"
include Gosu

require_relative "data/core/core"
require_relative "data/core/constants"
require_relative "data/scripts/base"

require_relative "editor/GUI"
require_relative "editor/common"
require_relative "editor/room_editor"

class Main < Window
  def initialize(fullscreen=false)
    super(800, 600, fullscreen)
    self.caption="Licencjat: Edytor Pomieszczeń"
    $time=0
  end

  def update
    GUI::System.Update
    $sapphire_system.update
    $state.update
    $time+=1
  end

  def draw
    GUI::System.Draw
    $state.draw
    $sapphire_system.draw
  end
  
  def button_down(id)
    $sapphire_system.button_down = id
    $state.button_down(id) if $state.respond_to?(:button_down)
  end
  
  def button_up(id)
    $sapphire_system.button_up = id
    $state.button_up(id) if $state.respond_to?(:button_up)
  end
  
  def needs_cursor?
    true
  end
end

$screen = Main.new
$state = RoomBrowser.new
$screen.show