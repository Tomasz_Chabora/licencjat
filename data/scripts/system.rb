class MapScreen
  POSITION_COLOR = 0xff00ffff
  
  attr_reader :_removed, :needs_drawing, :needs_dim, :destination
  
  def initialize(world, pos_x, pos_y)
    @world = world
    @pos_x, @pos_y = pos_x, pos_y
    @screen_x, @screen_y = center_around(@pos_x, @pos_y)
    @percent = ($save[:discovered].length.to_f / @world.square_count * 100).round(2)
    @location = @world.get_room(pos_x, pos_y).area.location
    
    minx = miny = 9999
    maxx = maxy = -9999
    
    @world.rooms.each do |room|
      minx = [minx, room.x].min
      maxx = [maxx, room.x + room.w-1].max
      miny = [miny, room.y].min
      maxy = [maxy, room.y + room.h-1].max
    end
    
    @min_screen_x = center_around(minx, 0).first
    @max_screen_x = center_around(maxx, 0).first
    @min_screen_y = center_around(0, miny).last
    @max_screen_y = center_around(0, maxy).last
  end
  
  def update
    return @one_frame_hack = nil if @one_frame_hack
    
    if @warp_mode
      wrp = @warp_mode
      if key_press(:right)
        warp = @max_x
        @warps.each{|warp2| warp = warp2 if warp2.x > @warp_mode.x and warp2.x < warp.x}
        @warp_mode = warp
      elsif key_press(:left)
        warp = @min_x
        @warps.each{|warp2| warp = warp2 if warp2.x < @warp_mode.x and warp2.x > warp.x}
        @warp_mode = warp
      elsif key_press(:down)
        warp = @max_y
        @warps.each{|warp2| warp = warp2 if warp2.y > @warp_mode.y and warp2.y < warp.y}
        @warp_mode = warp
      elsif key_press(:up)
        warp = @min_y
        @warps.each{|warp2| warp = warp2 if warp2.y < @warp_mode.y and warp2.y > warp.y}
        @warp_mode = warp
      end
      
      snd("MenuSelect").play if @warp_mode != wrp
      
      @location = @warp_mode.area.location
      dx, dy = center_around(@warp_mode.x, @warp_mode.y)
      
      if distance(@screen_x, @screen_y, dx, dy) > 25
        @screen_x += offset_x(angle(@screen_x, @screen_y, dx, dy), 25)
        @screen_y += offset_y(angle(@screen_x, @screen_y, dx, dy), 25)
      else
        @screen_x, @screen_y = dx, dy
      end
      
      if key_press(:attack) or key_press(:jump) && @warp_mode == $state.map.room
        snd("MenuCancel").play
        @_removed = true
      elsif key_press(:jump)
        snd("MenuAccept").play
        @_removed = true
        @destination = @warp_mode
      end
    else
      @screen_x -= Room::TILE if key_hold(KbLeft) and @screen_x > @min_screen_x
      @screen_x += Room::TILE if key_hold(KbRight) and @screen_x < @max_screen_x
      @screen_y -= Room::TILE if key_hold(KbUp) and @screen_y > @min_screen_y
      @screen_y += Room::TILE if key_hold(KbDown) and @screen_y < @max_screen_y
    end
  end
  
  def draw
    translate(-@screen_x, -@screen_y) do
      tls("System/Position", Room::TILE)[$time/4 % 4].draw(@pos_x * Room::TILE, @pos_y * Room::TILE, 2, 1, 1, POSITION_COLOR)
      img("System/Warp").draw_rot((@warp_mode.x+0.5) * Room::TILE, (@warp_mode.y+0.5) * Room::TILE, 5, -$time*8) if @warp_mode
      if !@map
        @map = $screen.record(SCREEN_WIDTH, SCREEN_HEIGHT) do
          @world.rooms.each{|r| r.draw}
        end
      else
        @map.draw(0, 0, 1)
      end
    end
    
    fnt("Courier New", Room::TILE).draw("#{@percent}%", Room::TILE, Room::TILE, 3) if !@warp_mode
    fnt("Courier New", Room::TILE).draw_rel(@location.name, SCREEN_WIDTH/2, Room::TILE, 3, 0.5, 0)
  end
  
  def center_around(x, y)
    [x * Room::TILE - SCREEN_WIDTH/2, y * Room::TILE - SCREEN_HEIGHT/2]
  end
  
  def warp_mode
    @warp_mode = @world.get_room(@pos_x, @pos_y)
    @warps = $state.world.save_points.select{|room| $save[:discovered].include?([room.x, room.y])}
    
    @warps.each do |warp|
      if !@min_x or warp.x < @min_x.x
        @min_x = warp
      end
      if !@max_x or warp.x > @max_x.x
        @max_x = warp
      end
      if !@min_y or warp.y < @min_y.y
        @min_y = warp
      end
      if !@max_y or warp.y > @max_y.y
        @max_y = warp
      end
    end
      
    @one_frame_hack = true
    self
  end
end

class InventoryScreen
  COLUMNS = 10
  ROWS = 5
  SLOT_SIZE = 48
  
  def initialize
    @select = [0, 0]
  end
  
  def update
    select = @select.dup
    @select[0] +=1 if @select[0] < COLUMNS-1 and key_press(:right)
    @select[0] -=1 if @select[0] > 0 and key_press(:left)
    @select[1] +=1 if @select[1] < ROWS-1 and key_press(:down)
    @select[1] -=1 if @select[1] > 0 and key_press(:up)
    snd("MenuSelect").play if @select != select
  end
  
  def draw
    COLUMNS.times do |x|
      ROWS.times do |y|
        x1 = SCREEN_WIDTH/2 - COLUMNS * SLOT_SIZE/2 + x * SLOT_SIZE
        y1 = SCREEN_HEIGHT/2 - ROWS * SLOT_SIZE/2 + y * SLOT_SIZE
        img("System/InventorySlot").draw(x1, y1, 0)
        $save[:inventory][xy_to_i(x, y)].draw(x1 + 4, y1 + 4, 0) if $save[:inventory][xy_to_i(x, y)]
        img("System/InventorySelect").draw(x1, y1, 0) if @select == [x, y]
      end
    end
    
    if $save[:inventory][xy_to_i(*@select)]
      fnt("Courier New", SLOT_SIZE/2).draw_rel($save[:inventory][xy_to_i(*@select)].name, SCREEN_WIDTH/2, SCREEN_HEIGHT/2 - ROWS * SLOT_SIZE/2 - SLOT_SIZE, 1, 0.5, 0)
    else
      fnt("Courier New", SLOT_SIZE/2).draw_rel("Inventory", SCREEN_WIDTH/2, SCREEN_HEIGHT/2 - ROWS * SLOT_SIZE/2 - SLOT_SIZE, 1, 0.5, 0)
    end
  end
  
  def xy_to_i(x, y)
    x % COLUMNS + y * COLUMNS
  end
end

class RelicScreen
  def initialize
  end
  
  def update
  end
  
  def draw
    if $save[:abilities].empty?
      fnt("Courier New", 24).draw_rel("No acquired abilities", SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 1, 0.5, 0.5)
    else
      $save[:abilities].each.with_index do |ability, i|
        img("System/Relic Slot").draw(100, 96 + i * 48, 0)
        tls("Objects/Relics", 40)[Relic::ABILITIES.index(ability)].draw(100, 100 + i * 48, 1)
        split_lines(Relic::NAMES[ability], 190).each.with_index do |line, ii|
          fnt("Courier New", 24).draw(line, 155, 96 + i * 48 + ii * 24, 1)
        end
        split_lines(Relic::DESCRIPTIONS[ability], 358).each.with_index do |line, ii|
          fnt("Courier New", 24).draw(line, 347, 96 + i * 48 + ii * 24, 1)
        end
      end
    end
  end
  
  def split_lines(text, max)
    lines = []
    texts = text.split
    l = r = w = 0
    
    texts.each do |text|
      w += fnt("Courier New", 24).text_width(text + " ")
      
      if w > max
        lines << texts[l...r].join(" ")
        l = r
        w = 0
      else
        r += 1
      end
    end
    
    lines << texts[l..r].join(" ")
  end
end

class PopUp < Entity
  def initialize(x, y, type, args = {})
    @x, @y , @type = x, y, type
    @args = args
    @life = 16
    
    init
  end
  
  def update
    @y -= @life/4
    @life -= 1
    remove if @life == -1
  end
  
  def draw
    case @type
      when :text
      @args[:font].draw(@args[:text], @x, @y, HUD_Z, :align => :center)
    end
  end
end

class SpecialInfo < Entity
  def initialize(name, desc)
    @name, @desc = name, desc
    
    @color = Color.new(0x00ffffff)
    @text1 = @text2 = 0
    
    init
  end
  
  def update
    if !@closed and @color.alpha < 255
      @color.alpha = [@color.alpha + 8, 255].min
    elsif !@closed
      if @text1 < @name.length
        @text1 += 2
      elsif @text2 < @desc.length
        @text2 += 2
      else
        @closed = true if key_press
      end
    end
    
    if @closed and @color.alpha > 0
      @color.alpha = [@color.alpha - 8, 0].max
    elsif @closed
      remove
    end
  end
  
  def draw
    $state.absolute_display do
      img("System/InfoPopUp").draw(0, SCREEN_HEIGHT/2 - 75, HUD_Z, 1, 1, @color)
      
      fnt("Courier New", 25).draw_rel(@name[0..@text1], SCREEN_WIDTH/2, SCREEN_HEIGHT/2 - 50, HUD_Z, 0.5, 0, 1, 1, @closed ? @color : Color::WHITE) if @text1 > 0
      fnt("Courier New", 25).draw_rel(@desc[0..@text2], SCREEN_WIDTH/2, SCREEN_HEIGHT/2, HUD_Z, 0.5, 0.5, 1, 1, @closed ? @color : Color::WHITE) if @text2 > 0
    end
  end
end

class LocationInfo < Entity
  def initialize(name, color)
    @name = name
    @text = 0
    @color = Color::BLACK.dup
    @color2 = color.dup
    
    init
  end
  
  def update
    if @text < @name.length
      @text += 1
    else
      @color.alpha -= 3
      @color2.alpha -= 3
      remove if @color.alpha == 0
    end
  end
  
  def draw
    $state.absolute_display do
      fnt("DejaVu Sans", 32).draw_rel(@name[0..@text], SCREEN_WIDTH/2, SCREEN_HEIGHT/2, HUD_Z, 0.5, 0.5, 1, 1, @color) if @text > 0
      fnt("DejaVu Sans", 32).draw_rel(@name[0..@text], SCREEN_WIDTH/2-2, SCREEN_HEIGHT/2-2, HUD_Z, 0.5, 0.5, 1, 1, @color2) if @text > 0
    end
  end
end

class TheEnd
  attr_reader :needs_dim, :needs_drawing, :_removed
  
  def initialize
    @color = Color.new(0x00ffffff)
  end
  
  def update
    if @color.alpha < 255
      @color.alpha += 2
    else
      @_removed = true if key_press(KbReturn)
      $state = MainMenu.new if key_press(KbEscape)
    end
  end
  
  def draw
    fnt("data/fonts/WIZARDRY.TTF", 40).draw_rel("WORLD CONQUERED", SCREEN_WIDTH/2 + rand(2), SCREEN_HEIGHT/2 + rand(2), 1, 0.5, 0.5, 1, 1, @color)
    fnt("Courier New", 24).draw_rel("Press Enter to continue game", SCREEN_WIDTH/2, SCREEN_HEIGHT - 128, 0, 0.5, 0, 1, 1, @color) if $time%60 < 30
    fnt("Courier New", 24).draw_rel("Press Escape to return to menu", SCREEN_WIDTH/2, SCREEN_HEIGHT - 96, 0, 0.5, 0, 1, 1, @color) if $time%60 < 30
    fnt("Courier New", 24).draw_rel("Your time: #{($save[:time]/60/60/60).to_s.rjust(2, "0")} : #{($save[:time]/60/60%60).to_s.rjust(2, "0")} : #{($save[:time]/60%60).to_s.rjust(2, "0")}", SCREEN_WIDTH/2, SCREEN_HEIGHT/2 + 80, 0.5, 0.5, 0, 1, 1, @color)
  end
end

class Label < Entity
  attr_reader :type, :args
  
  def initialize(type, args = {})
    @type = type
    @args = args
    @life = 60
    
    case @type
      when :enemy
      return if @@labels.find{|label| label and label.type == :enemy and label.args[:enemy] == @args[:enemy]}
      @color = Color::RED
      when :item
      @color = Color::GREEN
    end
    
    @y = register_label(self)
    
    init
  end
  
  def update
    @life -= 1
    
    if @life == 0
      deregister_label(self)
      remove
    end
  end
  
  def draw
    $state.absolute_display do
      size = fnt("Courier New", 32).text_width(@args[:text]).div(8) + 1
      
      tls("System/Label", 8, 32, retro: true)[3].draw(SCREEN_WIDTH - 8, SCREEN_HEIGHT - @y*32, HUD_Z)
      tls("System/Label", 8, 32)[0].draw(SCREEN_WIDTH - (size+2) * 8, SCREEN_HEIGHT - @y*32, HUD_Z)
      tls("System/Label", 8, 32)[1].draw(SCREEN_WIDTH - (size+1) * 8, SCREEN_HEIGHT - @y*32, HUD_Z, size)
      tls("System/Label", 8, 32)[2].draw(SCREEN_WIDTH - (size+1) * 8, SCREEN_HEIGHT - @y*32, HUD_Z, size, 1, @color)
      
      if @type == :enemy
        tls("System/Label", 8, 32)[4].draw(SCREEN_WIDTH - (size+1) * 8, SCREEN_HEIGHT - @y*32, HUD_Z, size)
        tls("System/Label", 8, 32)[5].draw(SCREEN_WIDTH - (size+1) * 8, SCREEN_HEIGHT - @y*32, HUD_Z, size * [@args[:enemy].hp.to_f / @args[:enemy].max_hp, 0].max)
      end
      
      fnt("Courier New", 32).draw_rel(@args[:text], SCREEN_WIDTH - (size+2) * 4, SCREEN_HEIGHT - @y*32, HUD_Z, 0.5, 0)
    end
  end
  
  @@labels = []
  
  def register_label(label)
    @@labels.push(label).length
  end
  
  def deregister_label(label)
    @@labels[@@labels.index(label)] = nil
    @@labels.clear if @@labels.compact.empty?
  end
  
  def Label.refresh
    @@labels.compact.each{|l| l.init}
  end
end

class BoxCollider
  attr_reader :x, :y, :w, :h
  
  def initialize(x, y, w, h)
    @x, @y, @w, @h = x, y, w, h
  end
  
  def move(x, y)
    @x, @y = x, y
  end
  
  def resize(w, h)
    @w, @h = w, h
  end
  
  def collides?(collider)
    collider.x < @x + @w and collider.x + collider.w > @x and collider.y < @y + @h and collider.y + collider.h > @y
  end
end