TILE = 24
GUI::System.Init

class SegmentPreview
  attr_accessor :x, :y, :grid
  attr_writer :darken, :disabled
  
  def initialize(grid, x, y, small = false)
    @x, @y, @small = x, y, small
    @grid = grid
  end
  
  def draw
    return if @disabled
    s = @small ? 0.25 : 1
    scale(s) do
      SegmentBase::HEIGHT.times do |y|
        SegmentBase::WIDTH.times do |x|
            tls("../../editor/Images/Geometry", TILE)[0].draw(@x/s + x*TILE, @y/s + y*TILE, 0, 1, 1, 0xff404040)
            t = x % SegmentBase::WIDTH + y * SegmentBase::WIDTH
            tile = @grid[t]
            if @special
              tls("../../editor/Images/Markers", TILE)[SegmentEdit::LAYER_TYPES.index(@special)].draw(@x/s + x*TILE, @y/s + y*TILE, 0) if tile
            else
              tls("../../editor/Images/Geometry", TILE)[tile ? tile+1 : 0].draw(@x/s + x*TILE, @y/s + y*TILE, 0, 1, 1, @darken ? 0xff404040 : @marker == t ? 0xff00ffff : 0xffffffff) if tile or @marker == t
            end
        end
      end
    end
  end
  
  def edit(tile)
    @darken = nil
    @special = (tile.is_a?(String) ? tile : false)
    x = $screen.mouse_x-@x
    y = $screen.mouse_y-@y
    coord = (x.to_i/TILE) % SegmentBase::WIDTH + (y.to_i/TILE) * SegmentBase::WIDTH
    
    return if x<0 or y<0 or y>=15*TILE or x>=20*TILE or coord >= SegmentBase::WIDTH * SegmentBase::HEIGHT
    
    if key_hold(MsLeft)
      @grid[coord] = (@special ? true : tile)
    elsif key_hold(MsRight)
      @grid[coord] = (@grid != $segment.grid ? nil : 0)
    end
    
    if key_press(MsMiddle)
      @marker = coord
    end
    
    if key_press(KbC)
      @@clipboard = @grid.dup
    end
    
    if key_press(KbV) and defined? @@clipboard
      @grid.replace @@clipboard
    end
  end
end

class Metadata
  def self.load(table, id)
    @@data = File.open("editor/metadata") {|file| Marshal.load(file)} unless defined? @@data
    
    @@data[table] ||= []
    @@data[table][id]
  end
  
  def self.store(table, id, data)
    @@data = File.open("editor/metadata") {|file| Marshal.load(file)} unless defined? @@data
    
    @@data[table] ||= []
    @@data[table][id] = data
    
    Marshal.dump(@@data, f=File.new("editor/metadata", 'w'))
    f.close
  end
end