class Load
  DEFAULT_CONTROLS = {left: KbLeft, right: KbRight, down: KbDown, up: KbUp, jump: KbLeftControl, attack: KbLeftShift, menu: KbReturn, special: KbSpace}
  DEFAULT_CONFIG = {controls: DEFAULT_CONTROLS, fullscreen: false}
  
  def initialize
    if File.exists?("data/config")
      File.open("data/config") {|file| $config = Marshal.load(file)}
    else
      $config = DEFAULT_CONFIG.dup
    end
    bind_keys $config[:controls]
    $screen.fullscreen = $config[:fullscreen]
    
    dirs=['gfx', 'sfx', 'music']
    @gfx=[]
    @sfx=[]
    @mfx=[]
    
    while !dirs.empty?
      (Dir.entries('data/'+dirs.first)-['.','..']).each{|file|
      if Dir.exists?('data/'+dirs.first+'/'+file)
        dirs << (dirs.first+'/'+file)
      else
        case dirs.first.split('/').first
          when 'gfx'
          @gfx << (dirs.first.gsub('gfx', '')+'/'+file) if file.end_with?(".png")
          when 'sfx'
          @sfx << (dirs.first.gsub('sfx', '')+'/'+file) if file.end_with?(".ogg")
          when 'music'
          @mfx << (dirs.first.gsub('music', '')+'/'+file) if file.end_with?(".ogg")
        end
      end}
      dirs.shift
    end
    
    fnt('System/Numbers', 12, 16).define_characters '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '+'
  end
  
  def update
    dt = milliseconds
    while milliseconds - dt < 16
      if !@gfx.empty?
        img(@gfx.pop.chomp('.png'))
      elsif !@sfx.empty?
        snd(@sfx.pop.chomp('.ogg'))
      elsif !@mfx.empty?
        msc(@mfx.pop.chomp('.ogg'))
      else
        return $state = MainMenu.new
      end
    end
  end
  
  def draw
    fnt("Courier New", Room::TILE).draw("Loading resources#{'.' * ($time/8%4)}", Room::TILE, SCREEN_HEIGHT - Room::TILE*2, 0)
  end
end

class InGame < State
  attr_reader :world, :player, :time, :screen_x, :screen_y
  attr_accessor :map
  
  def groups
    [[:enemy]]
  end
  
  def setup(world)
    @time = 0
    @world = world
    
    @player = Player.new
    
    if $save
      Map.new(@world.get_room(*$save[:save_point]))
      save_point = find{|ent| ent.class == Save}
      @player.x = save_point.x + 30
      @player.y = save_point.y + 60
    else
      Map.new(@world.rooms.first)
      $save = create_save
    end
    stop_music
  end
  
  def pre
    @current_square = get_map_square
    
    if @player.center_x < 0
      change_room(@map.room.get_exit(L, @player.center_y / SCREEN_HEIGHT))
    elsif @player.center_x > @map.width
      change_room(@map.room.get_exit(R, @player.center_y / SCREEN_HEIGHT))
    elsif @player.center_y < -16
      change_room(@map.room.get_exit(U, @player.center_x / SCREEN_WIDTH))
    elsif @player.center_y > @map.height
      change_room(@map.room.get_exit(D, @player.center_x / SCREEN_WIDTH))
    end
  end
  
  def post
    @screen_x = [[@player.center_x - SCREEN_WIDTH/2, 0].max, @map.width - SCREEN_WIDTH].min
    @screen_y = [[@player.center_y_absolute - SCREEN_HEIGHT/2, 0].max, @map.height - SCREEN_HEIGHT].min
    
    if @info
      @info.update
      if @info._removed
        @info = @no_update = nil
      else
        return
      end
    end
    
    if @menu
      if key_press(:menu)
        snd("MenuCancel").play
        @menu = @no_update = nil
      elsif key_press(:special)
        snd("MenuAccept").play
        @menu.rotate!
      else
        @menu.first.update
        return
      end
    elsif !@menu and key_press(:menu)
      snd("MenuOpen").play
      @menu = []
      @menu << MapScreen.new(@world, *get_map_square)
      @menu << InventoryScreen.new
      @menu << RelicScreen.new
      @no_update = true
    end
    @time += 1
    $save[:time] += 1
    
    current_square = get_map_square
    if @current_square != current_square and !$save[:discovered].include?(current_square)
      $save[:discovered] << current_square
      
      if $save[:discovered].length == @world.square_count
        info(TheEnd.new)
      end
    end
    
    if $save[:hp] <= 0
      $state = GameOver.new
    end
  end
  
  def back
    @map.draw_background(@screen_x, @screen_y)
    
    $screen.translate(-@screen_x, -@screen_y) do
      @map.draw_tiles
    end
  end
  
  def front
    if @menu or @info
      draw_quad(0, 0, c = 0xf0000000, SCREEN_WIDTH, 0, c, SCREEN_WIDTH, SCREEN_HEIGHT, c, 0, SCREEN_HEIGHT, c, 0) if !@info or @info.respond_to?(:needs_dim)
      
      if @menu
        @menu.first.draw
        fnt("Courier New", 24).draw_rel(key_name(key_bound(:special)) + " >>>", SCREEN_WIDTH - 24, SCREEN_HEIGHT - 48, 2, 1, 0) if $time%60 < 30
        fnt("Courier New", 24).draw_rel("#{($save[:time]/60/60/60).to_s.rjust(2, "0")} : #{($save[:time]/60/60%60).to_s.rjust(2, "0")} : #{($save[:time]/60%60).to_s.rjust(2, "0")}", 24, SCREEN_HEIGHT - 48, 2, 0, 0)
      elsif @info.respond_to?(:needs_drawing)
        @info.draw
      end
    else
      img("System/HUD").draw(8, 8, 0)
      img("System/Bar").subimage(0, 0 , (169.0 * $save[:hp] / $save[:max_hp]).ceil, 12).draw(37, 18, 0) if $save[:hp] > 0
      fnt("System/Numbers", 12, 16).draw([$save[:hp], 0].max, 24, 24, 0, :align => :center)
    end
  end
  
  def change_room(exit)
    old_map = @map
    reset
    Map.new(@world.get_room(@map.room.x + (@player.center_x / SCREEN_WIDTH.to_f).floor, @map.room.y + (@player.center_y / SCREEN_HEIGHT.to_f).floor))
    @player.init
    
    case exit.direction
      when U
      @player.x += (old_map.room.x - @map.room.x) * SCREEN_WIDTH
      @player.y += @map.height
      when R
      @player.x -= old_map.width
      @player.y += (old_map.room.y - @map.room.y) * SCREEN_HEIGHT
      when D
      @player.x += (old_map.room.x - @map.room.x) * SCREEN_WIDTH
      @player.y -= old_map.height
      when L
      @player.x += @map.width
      @player.y += (old_map.room.y - @map.room.y) * SCREEN_HEIGHT
    end
    
    if !$save[:locations].include?(@map.room.area.location.name)
      $save[:locations] << @map.room.area.location.name
      info(LocationInfo.new(@map.room.area.location.name, @map.room.area.location.get_color))
    end
    
    loop_music(@map.room.area.location.base.music)
  end
  
  def solid?(x, y, platform = false)
    x,y=x.to_i,y.to_i
    return solid?(x,0) if y<0
    return solid?(x,@map.height-1) if y>=@map.height
    return solid?(0,y) if x<0
    return solid?(@map.width-1,y) if x>=@map.width
    
    case @map.grid[[x/Tile::SIZE, y/Tile::SIZE]]
      when :wall
      return true
      when :platform
      return platform && y%Tile::SIZE==0
      when :water
      return $water_walking && platform && y%Tile::SIZE==20 && @map.grid[[x/Tile::SIZE, y/Tile::SIZE-1]] == :air
      when :slope1
      return x%Tile::SIZE >= Tile::SIZE - y%Tile::SIZE
      when :slope2
      return x%Tile::SIZE <= y%Tile::SIZE
      when :slope3
      return x%Tile::SIZE >= y%Tile::SIZE
      when :slope4
      return x%Tile::SIZE <= Tile::SIZE - y%Tile::SIZE
    end
  end
  
  def water?(x, y)
    return water?(x,0) if y<0
    return water?(x,@map.height-1) if y>=@map.height
    return water?(0,y) if x<0
    return water?(@map.width-1,y) if x>=@map.width
    
    if @map.grid[[x/Tile::SIZE,y/Tile::SIZE-1]] == :air
      @map.grid[[x/Tile::SIZE,(y-20)/Tile::SIZE]] == :water
    else
      @map.grid[[x/Tile::SIZE,y/Tile::SIZE]] == :water
    end
  end
  
  def create_save
    {hp: 200, max_hp: 200, discovered: [[0, 0]], collected: [], inventory: [], abilities: [], locations: [], time: 0}
  end
  
  def get_map_square
    [@map.room.x + @player.center_x / SCREEN_WIDTH, @map.room.y + @player.center_y / SCREEN_HEIGHT]
  end
  
  def absolute_display
    translate(@screen_x, @screen_y) do
      yield
    end
  end
  
  def get_collected(x, y)
    $save[:collected].include? @map.map_id + "_#{x}_#{y}"
  end
  
  def add_collected(x, y)
    $save[:collected] << @map.map_id + "_#{x}_#{y}"
  end
  
  def info(infoobj)
    @info = infoobj
    @no_update = true
  end
  
  def reset
    super
    Label.refresh
  end
end

class MainMenu
  def initialize
    @p1x = rand(800)
    @p2x = rand(800)
    @select = 0
    msc("Dungeon Groove").play(true)
  end
  
  def update
    if @select_key
      if key = key_press
        snd("MenuAccept").play
        bind_keys({@select_key => key}, @options[:controls])
        @select_key = nil
      end
      return
    end
    
    if key_press(KbUp) and @select > 0
      snd("MenuSelect").play
      @select -= 1
    elsif key_press(KbDown) and @select < (@options ? controls.length + 3 : texts.length-1)
      snd("MenuSelect").play
      @select += 1
    end
    
    if key_press(KbReturn)
      if @options
        if @select < controls.length
          snd("MenuAccept").play
          @select_key = controls.keys[@select]
        else
          if @select == controls.length
            snd("MenuAccept").play
            $config = @options
            File.open("data/config", "w") {|file| Marshal.dump($config, file)}
            
            bind_keys $config[:controls]
            $screen.fullscreen = $config[:fullscreen]
          elsif @select == controls.length+2
            snd("MenuCancel").play
            $config = Load::DEFAULT_CONFIG.dup
            File.delete("data/config") if File.exists?("data/config")
            
            bind_keys $config[:controls]
            $screen.fullscreen = $config[:fullscreen]
          else
            snd("MenuCancel").play
          end
          
          @options = nil
          @select = 0
        end
      else
        case texts[@select][0]
          when "C"
          snd("MenuAccept").play
          save = File.open("data/save")
          $save = Marshal.load(save)
          $state = InGame.new(Marshal.load(save))
          
          when "S"
          snd("MenuAccept").play
          $save = nil
          return Generator.new
          
          when "O"
          snd("MenuAccept").play
          @options = $config.dup
          @select = 0
          
          when "E"
          $screen.close
        end
      end
    elsif key_press(KbEscape)
      if @options
        snd("MenuCancel").play
        @options = nil
        @select = 2
      else
        @select = 3
      end
    end
  end
  
  def draw
    img("System/MainMenuBottom").draw(@p1x % 800 - 800, 0, 0)
    img("System/MainMenuBottom").draw(@p1x % 800, 0, 0)
    img("System/MainMenu").draw(@p2x % 800, 0, 0)
    img("System/MainMenu").draw(@p2x % 800 - 800, 0, 0)
    
    @p1x -= 1
    @p2x += 1
    
    fnt("data/fonts/WIZARDRY.TTF", 40).draw_rel("INFINITE DUNGEONS", SCREEN_WIDTH/2 + rand(2), 40 + rand(2), 1, 0.5, 0)
    
    if @options
      fnt("Courier New", 24).draw_rel("Controls", SCREEN_WIDTH/2, SCREEN_HEIGHT / 2 - 32, 0, 0.5, 0)
      
      y = 0
      ii = 0
      controls.values.each.with_index do |text, i|
        txt = (@select_key == controls.key(text) ? "<press new key>" : key_name(key_bound(controls.key(text), @options[:controls])))
        fnt("Courier New", 24).draw_rel(text.rjust(20, " ") + " ... " + txt.ljust(20, " "), SCREEN_WIDTH/2, y = SCREEN_HEIGHT / 2 + i*24, 0, 0.5, 0, 1 + (@select == i ? rand/8 : 0), 1 + (@select == i ? rand/8 : 0), @select == i ? Color::YELLOW : Color::WHITE)
        ii = i
      end
      
      ii+=1
      fnt("Courier New", 24).draw_rel("Accept", SCREEN_WIDTH/2, y += 32, 0, 0.5, 0, 1 + (@select == ii ? rand/8 : 0), 1 + (@select == ii ? rand/8 : 0), @select == ii ? Color::YELLOW : Color::WHITE)
      ii+=1
      fnt("Courier New", 24).draw_rel("Cancel", SCREEN_WIDTH/2, y += 24, 0, 0.5, 0, 1 + (@select == ii ? rand/8 : 0), 1 + (@select == ii ? rand/8 : 0), @select == ii ? Color::YELLOW : Color::WHITE)
      ii+=1
      fnt("Courier New", 24).draw_rel("Restore defaults", SCREEN_WIDTH/2, y += 24, 0, 0.5, 0, 1 + (@select == ii ? rand/8 : 0), 1 + (@select == ii ? rand/8 : 0), @select == ii ? Color::YELLOW : Color::WHITE)
    else
      texts.each.with_index do |text, i|
        fnt("Courier New", 24).draw_rel(text, SCREEN_WIDTH/2, SCREEN_HEIGHT * 2/3 + i*24, 0, 0.5, 0, 1 + (@select == i ? rand/8 : 0), 1 + (@select == i ? rand/8 : 0), @select == i ? Color::YELLOW : Color::WHITE)
      end
      
      if texts.length == 4 and @select == 1
        fnt("Courier New", 16).draw_rel("WARNING: When you start new game and save, your previous progress will be lost", SCREEN_WIDTH/2, SCREEN_HEIGHT - 24, 0, 0.5, 0, 1, 1, Color::RED)
      end
    end
  end
  
  def texts
    texts = "Start new game", "Options", "Exit"
    texts.unshift("Continue game") if File.exists?("data/save")
    texts
  end
  
  def controls
    {left: "Left", right: "Right", up: "Up", down: "Down", jump: "Jump", attack: "Attack", menu: "Map screen", special: "Special action"}
  end
end

class GameOver
  def initialize
    stop_music
  end
  
  def update
    if key_press KbReturn
      if File.exists?("data/save")
        save = File.open("data/save")
        $save = Marshal.load(save)
        $state = InGame.new(Marshal.load(save))
      else
        $save = nil
        return Generator.new
      end
    end
    
    $screen.close if key_press(KbEscape)
  end
  
  def draw
    fnt("data/fonts/WIZARDRY.TTF", 80).draw_rel("RIP", SCREEN_WIDTH/2 + rand(4), SCREEN_HEIGHT/2 + rand(4), 1, 0.5, 0.5)
    
    if File.exists?("data/save")
      fnt("Courier New", 24).draw_rel("Press Enter to continue from last save", SCREEN_WIDTH/2, SCREEN_HEIGHT - 48, 0, 0.5, 0) if $time%60 < 30
    else
      fnt("Courier New", 24).draw_rel("Press Enter to start a new game", SCREEN_WIDTH/2, SCREEN_HEIGHT - 48, 0, 0.5, 0) if $time%60 < 30
    end
    fnt("Courier New", 24).draw_rel("Press Escape to quit", SCREEN_WIDTH/2, SCREEN_HEIGHT - 24, 0, 0.5, 0) if $time%60 < 30
  end
end