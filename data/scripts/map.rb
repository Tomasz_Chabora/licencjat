class Map
  TILE_TYPES = :air, :wall, :platform, :slope1,:slope2,:slope3,:slope4,:water
  
  attr_reader :grid, :room, :map_id
  
  def initialize(room)
    @room = room
    @grid = {}
    @secondary_grid = {}
    @tiles = []
    @map_id = "#{room.x}_#{room.y}"
    $state.world.reset_random(@room.unique_id)
    $state.map = self
    
    Save.new(10, 10) if @room.x == 0 and @room.y == 0
    
    @room.segments.each_key do |s| x1,y1 = s[0] * SegmentBase::WIDTH, s[1] * SegmentBase::HEIGHT
      grid = @room.segments[s].get_grid(@room, s[0], s[1])
      grid.each_index do |t| x2,y2 = t % SegmentBase::WIDTH, t / SegmentBase::WIDTH
        @grid[[x1+x2, y1+y2]] = Map.tile_type(grid[t])
      end
      
      if layer = @room.segments[s].base.layers.find{|layer| layer.type == SegmentLayer::T_SECONDARY}
        layer.grid.each.with_index do |g, t| x2,y2 = t % SegmentBase::WIDTH, t / SegmentBase::WIDTH
          @secondary_grid[[x1+x2, y1+y2]] = true if g
        end
      end
    end
    
    @room.placeholders.each do |placeholder|
      process_placeholder(placeholder)
    end
    
    tileset = @room.area.tileset
    alt_tileset = @room.area.base.tilesets.find{|tileset| tileset.split.last == "secondary"}
    alt_tileset = alt_tileset.split.first if alt_tileset
    
    number_of_details = Tile.tileset_details(tileset)
    number_of_details2 = Tile.tileset_details(alt_tileset) if alt_tileset
    @grid.each_pair do |pos, val| next if val == :air
      if val == :water
        @tiles << Water.new(pos[0] * Tile::SIZE, pos[1] * Tile::SIZE, @grid[[pos[0], pos[1]-1]] == :air)
        @tiles << Water.new((pos[0]-1) * Tile::SIZE, pos[1] * Tile::SIZE, @grid[[pos[0]-1, pos[1]-1]] == :air) if [:slope2, :slope4].include?(@grid[[pos[0]-1, pos[1]]])
        @tiles << Water.new((pos[0]+1) * Tile::SIZE, pos[1] * Tile::SIZE, @grid[[pos[0]+1, pos[1]-1]] == :air) if [:slope1, :slope3].include?(@grid[[pos[0]+1, pos[1]]])
      else
        alt = @secondary_grid[pos]
        @tiles << Tile.new(pos[0] * Tile::SIZE, pos[1] * Tile::SIZE, alt ? alt_tileset : tileset, val, Array.new(5) {|i| $state.world.rand((alt ? number_of_details2 : number_of_details)[i])}, alt)
      end
    end
    
    @background = Background.new(@room.base.background)
  end
  
  def process_placeholder(placeholder)
    case placeholder.type
      when :enemy
      return if @room.save_point
      Enemy.get_enemy(placeholder.data[:type]).new(placeholder.x, placeholder.y)
      
      when :item
      placeholder.data[:item].init(placeholder.x, placeholder.y)
      
      when :save
      Save.new(placeholder.x, placeholder.y) if @room.save_point
    end
  end
  
  def draw_background(screen_x, screen_y)
    if @room == $state.world.rooms.first
      img("Backgrounds/StartPoint").draw(0, 0, 0)
    else
      @background.draw(screen_x, screen_y)
    end
  end
  
  def draw_tiles
    if not @tilemap
      @tilemap = record(Tile::SIZE, Tile::SIZE) do
        @tiles.each{|t| t.draw}
      end
      @tilemap.draw(0, 0, Tile::Z)
    else
      @tilemap.draw(0, 0, Tile::Z)
    end
  end
  
  def width
    @room.w * SegmentBase::WIDTH * Tile::SIZE
  end
  
  def height
    @room.h * SegmentBase::HEIGHT * Tile::SIZE
  end
  
  def self.tile_type(i)
    TILE_TYPES[i]
  end
  
  def check_secondary(x, y, sec)
    return true if x < 0 or y < 0 or x >= @room.w * SegmentBase::WIDTH or y >= @room.h * SegmentBase::HEIGHT
    
    !!@secondary_grid[[x, y]] == !!sec
  end
end

class Tile
  attr_reader :tileset
  
  def initialize(x,y,tileset,type,details,secondary)
    @x, @y, @tileset, @type, @details, @secondary = x, y, tileset, type, details, secondary
  end
  
  def draw(top = false)
    case @type
      when :wall
      tls("Tiles/#{@tileset}/Middle",SIZE)[@details[0]].draw(@x,@y,Z)
      
      w = $state.map.width - SIZE
      h = $state.map.height - SIZE
      
      tls("Tiles/#{@tileset}/Corner",SIZE_2,SIZE_2)[0].draw(@x,@y,Z) if @x>0 and !find(-SIZE,0) and !find(0,-SIZE)
      tls("Tiles/#{@tileset}/Corner",SIZE_2,SIZE_2)[1].draw(@x+SIZE_2,@y,Z) if @x<w and !find(SIZE,0) and !find(0,-SIZE)
      tls("Tiles/#{@tileset}/Corner",SIZE_2,SIZE_2)[2].draw(@x,@y+SIZE_2,Z) if @x>0 and !find(-SIZE,0) and !find(0,SIZE)
      tls("Tiles/#{@tileset}/Corner",SIZE_2,SIZE_2)[3].draw(@x+SIZE_2,@y+SIZE_2,Z) if @x<w and !find(SIZE,0) and !find(0,SIZE)
      
      tls("Tiles/#{@tileset}/Top",SIZE_2)[@details[1]*2].draw(@x,@y,Z) if @y>0 and find(-SIZE,0) || @x==0 and !find(0,-SIZE)
      tls("Tiles/#{@tileset}/Top",SIZE_2)[@details[1]*2+1].draw(@x+SIZE_2,@y,Z) if @y>0 and find(SIZE,0) || @x==w and !find(0,-SIZE)
      tls("Tiles/#{@tileset}/Bottom",SIZE_2)[@details[3]*2].draw(@x,@y+SIZE_2,Z) if @y<h and find(-SIZE,0) || @x==0 and !find(0,SIZE)
      tls("Tiles/#{@tileset}/Bottom",SIZE_2)[@details[3]*2+1].draw(@x+SIZE_2,@y+SIZE_2,Z) if @y<h and find(SIZE,0) || @x==w and !find(0,SIZE)
      
      tls("Tiles/#{@tileset}/Left",SIZE_2)[@details[4]*2].draw(@x,@y,Z+1) if @x>0 and find(0,-SIZE) and !find(-SIZE,0)
      tls("Tiles/#{@tileset}/Left",SIZE_2)[@details[4]*2+1].draw(@x,@y+SIZE_2,Z+1) if @x>0 and find(0,SIZE) and !find(-SIZE,0)
      tls("Tiles/#{@tileset}/Right",SIZE_2)[@details[2]*2].draw(@x+SIZE_2,@y,Z+1) if @x<w and find(0,-SIZE) and !find(SIZE,0)
      tls("Tiles/#{@tileset}/Right",SIZE_2)[@details[2]*2+1].draw(@x+SIZE_2,@y+SIZE_2,Z+1) if @x<w and find(0,SIZE) and !find(SIZE,0)
      
      tls("Tiles/#{@tileset}/Corner",SIZE_2)[6].draw(@x+SIZE_2,@y,Z) if find3(0,-SIZE) and find3(SIZE,0) and !find(SIZE,-SIZE)
      tls("Tiles/#{@tileset}/Corner",SIZE_2)[7].draw(@x,@y,Z) if find3(0,-SIZE) and find3(-SIZE,0) and !find(-SIZE,-SIZE)
      tls("Tiles/#{@tileset}/Corner",SIZE_2)[4].draw(@x+SIZE_2,@y+SIZE_2,Z) if find3(0,SIZE) and find3(SIZE,0) and !find(SIZE,SIZE)
      tls("Tiles/#{@tileset}/Corner",SIZE_2)[5].draw(@x,@y+SIZE_2,Z) if find3(0,SIZE) and find3(-SIZE,0) and !find(-SIZE,SIZE)
      
      when :platform
      tls("Tiles/#{@tileset}/Platform",SIZE_2)[0].draw(@x,@y,Z) if !find4(-SIZE,0)
      tls("Tiles/#{@tileset}/Platform",SIZE_2)[1].draw(@x,@y,Z) if find4(-SIZE,0)
      tls("Tiles/#{@tileset}/Platform",SIZE_2)[2].draw(@x+SIZE_2,@y,Z) if find4(SIZE,0)
      tls("Tiles/#{@tileset}/Platform",SIZE_2)[3].draw(@x+SIZE_2,@y,Z) if !find4(SIZE,0)
      
      when :slope1
      tls("Tiles/#{@tileset}/SlopeL",SIZE,SIZE2)[0].draw(@x,@y,Z+10)
      
      when :slope2
      tls("Tiles/#{@tileset}/SlopeR",SIZE,SIZE2)[0].draw(@x,@y,Z+10)
      
      when :slope3
      tls("Tiles/#{@tileset}/Slope2R",SIZE,SIZE2)[0].draw(@x,@y-SIZE,Z+9)
      
      when :slope4
      tls("Tiles/#{@tileset}/Slope2L",SIZE,SIZE2)[0].draw(@x,@y-SIZE,Z+9)
      
      when :water
      tls("Environment/Water",SIZE,SIZE)[top ? 0 : 1].draw(@x,@y,Z+10)
    end
  end
  
  def find(x,y)
    ![:air,:water,:lava,:platform].include?($state.map.grid[[(@x+x)/SIZE,(@y+y)/SIZE]]) and $state.map.check_secondary((@x+x)/SIZE, (@y+y)/SIZE, @secondary)
  end
  
  def find2(x,y)
    $state.map.grid[[(@x+x)/SIZE,(@y+y)/SIZE]] and $state.map.check_secondary((@x+x)/SIZE, (@y+y)/SIZE, @secondary)
  end
  
  def find3(x,y)
    ![:air,:slope1,:slope2,:slope3,:slope4,:water,:lava,:platform].include? $state.map.grid[[(@x+x)/SIZE,(@y+y)/SIZE]] and $state.map.check_secondary((@x+x)/SIZE, (@y+y)/SIZE, @secondary)
  end
  
  def find4(x,y)
    [:water,:lava,:platform].include? $state.map.grid[[(@x+x)/SIZE,(@y+y)/SIZE]]
  end
  
  def reset;@predraw=nil;end
  
  def self.tileset_details(tileset)
    [
    tls("Tiles/#{tileset}/Middle", SIZE),
    tls("Tiles/#{tileset}/Top", SIZE, SIZE_2),
    tls("Tiles/#{tileset}/Right", SIZE_2, SIZE),
    tls("Tiles/#{tileset}/Bottom", SIZE, SIZE_2),
    tls("Tiles/#{tileset}/Left", SIZE_2, SIZE)
    ].collect{|tiles| tiles.length}
  end
end

class Water
  def initialize(x, y, top)
    @x, @y, @top = x, y, top
  end
  
  def draw
    tls("Environment/Water", Tile::SIZE, Tile::SIZE)[@top ? 0 : 1].draw(@x, @y, Tile::Z - 1, 1, 1, 0x80ffffff)
  end
end

class Background
  def initialize(id)
    @id = id
    
    if @id > -1
      @sequences = []
      base.layers.each do |layer|
        @sequences << create_sequence(layer)
      end
    end
  end
  
  def draw(screen_x, screen_y)
    draw_quad(0, 0, c = 0xFF1F75FE, SCREEN_WIDTH, 0, c, SCREEN_WIDTH, SCREEN_HEIGHT, c, 0, SCREEN_HEIGHT, c, 0)
    if @id > -1
      base.layers.each.with_index do |layer, index|
        case layer.position
          when BackgroundLayer::P_BOTTOM
          x1 = 0
          @sequences[index].each do |image_id|
            image = img("Backgrounds/#{layer.image.chop + (image_id+97).chr}")
            
            if !layer.loop_x
              x1 = (SCREEN_WIDTH/image.width+2)/2 * image.width
            end
            
            image.draw(x1 - (screen_x * layer.scroll_x*0.001).floor, SCREEN_HEIGHT - image.height - ((screen_y - ($state.map.room.h-1) * SCREEN_HEIGHT) * layer.scroll_y*0.001).floor, 0)
            x1 += image.width
          end
          
          when BackgroundLayer::P_TOP
          x1 = 0
          @sequences[index].each do |image_id|
            image = img("Backgrounds/#{layer.image.chop + (image_id+97).chr}")
            
            if !layer.loop_x
              x1 = (SCREEN_WIDTH/image.width+2)/2 * image.width
            end
            
            image.draw(x1 - (screen_x * layer.scroll_x*0.001).floor, ((screen_y - ($state.map.room.h-1) * SCREEN_HEIGHT) * layer.scroll_y*0.001).floor, 0)
            x1 += image.width
          end
            
          when BackgroundLayer::P_FILL
          x1 = 0
          y1 = 0
          @sequences[index].each do |image_id|
            image = img("Backgrounds/#{layer.image.chop + (image_id+97).chr}")
            
            image.draw(x1 - (screen_x * layer.scroll_x*0.001).floor, y1 - (screen_y * layer.scroll_y*0.001).floor, 0)
            x1 += image.width
            
            if x1 >= $state.map.room.w * SCREEN_WIDTH
              x1 = 0
              y1 += image.height
            end
          end
        end
      end
    end
  end
  
  def create_sequence(layer)
    sequence = []
    
    case layer.position
      when BackgroundLayer::P_BOTTOM, BackgroundLayer::P_TOP
      if layer.loop_x
        x = 0
        while x < $state.map.room.w * SCREEN_WIDTH
          sequence << $state.world.rand(layer.image[-1].ord-96)
          image = img("Backgrounds/#{layer.image.chop + (sequence.last+97).chr}")
          
          x += image.width
        end
      else
        sequence << $state.world.rand(layer.image[-1].ord-96)
      end
      
      when BackgroundLayer::P_FILL
      y = 0
      while y < $state.map.room.h * SCREEN_HEIGHT
        x = 0
        while x < $state.map.room.w * SCREEN_WIDTH
          sequence << $state.world.rand(layer.image[-1].ord-96)
          image = img("Backgrounds/#{layer.image.chop + (sequence.last+97).chr}")
          
          x += image.width
        end
        y += image.height
      end
    end
    
    sequence
  end
  
  def base
    BackgroundBase.get(@id)
  end
end