class ItemBase
  attr_writer :type, :names, :replacements, :layers
  
  def self.refresh
    @@items = []
    (Dir.entries("data/base/Items") - ['.','..']).each do |id|
      @@items << Marshal.load(f = File.new("data/base/Items/#{id}"))
      f.close
    end
  end
end

class ItemEdit
  def initialize
    @return = GUI::Button.new(720, 4, "Return")
    @save = GUI::Button.new(4, 4, "Save")
    GUI::Label.new(48, 4, "Item " + $item_id.to_s.rjust(4, "0"))
    @name = GUI::Textbox.new(192, 4, 32)
    @name.value = Metadata.load(:item_name, $item_id)
    
    GUI::Label.new(8, 32, "Names")
    @names = GUI::Textbox.new(8, 64, 64)
    @names.value = $item.names.join("; ")
    
    GUI::Label.new(600, 32, "Type")
    @type = GUI::Dropdown.new(600, 64, *ItemBase::TYPES.collect{|t| t.to_s.capitalize})
    @type.value = ItemBase::TYPES.index $item.type
    
    GUI::Label.new(8, 96, "Replacements")
    @add_rep = GUI::Button.new(8, 128, "+")
    @delete_rep = GUI::Button.new(48, 128, "-")
    
    @rep_edits = []
    $item.replacements.each.with_index do |rep, i|
      @rep_edits << new_rep_edit(i)
      @rep_edits.last[0].value = rep[:key]
      @rep_edits.last[1].value = rep[:strings].join("; ")
    end
    
    GUI::Label.new(8, 300, "Layers")
    @add_layer = GUI::Button.new(8, 332, "+")
    @delete_layer = GUI::Button.new(48, 332, "-")
    
    @layer_edits = []
    $item.layers.each.with_index do |layer, i|
      @layer_edits << new_layer_edit(i)
      @layer_edits.last.value = layer.join("; ")
    end
  end
  
  def new_rep_edit(i)
    [GUI::Textbox.new(80, 128 + i*24, 4),
        GUI::Textbox.new(140, 128 + i*24, 32)]
  end
  
  def new_layer_edit(i)
    GUI::Textbox.new(140, 332 + i*24, 32)
  end
  
  def update
    if @return.value
      GUI::System.Clear
      ItemBase.refresh
      
      return $state = ItemBrowser.new
    end
    
    if @add_rep.value
      @rep_edits << new_rep_edit(@rep_edits.length)
     end
      
    if @delete_rep.value and !@rep_edits.empty?
      @rep_edits.last.each{|e| e.remove}
      @rep_edits.pop
    end
    
    if @add_layer.value
      @layer_edits << new_layer_edit(@layer_edits.length)
     end
      
    if @delete_layer.value and !@layer_edits.empty?
      @layer_edits.last.remove
      @layer_edits.pop
    end
    
    save if @save.value
  end
  
  def draw
  end
  
  def save
    Metadata.store(:item_name, $item_id, @name.value)
    $item.replacements = @rep_edits.collect{|r| {:key => r[0].value, :strings => r[1].value.split(/;\s*/)}}
    $item.layers = @layer_edits.collect{|r| r.value.split(/;\s*/)}
    $item.names = @names.value.split(/;\s*/)
    $item.type = ItemBase::TYPES[@type.value]
    
    f = File.new("data/base/Items/" + $item_id.to_s.rjust(4, "0"), 'w')
    Marshal.dump($item, f)
    f.close
  end
end

class ItemBrowser
  def initialize
    @rooms = []
    @edits = []
    
    (ItemBase.count+1).times do |i|
      x = 4
      y = i*32
      
      if i == ItemBase.count
        y = i*32
        @edits << GUI::Button.new(4+x, 4+y, "New")
      else
        @edits << GUI::Button.new(x, 4+y, "Edit")
        GUI::Label.new(48+x, 4+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:item_name, i)}>")
      end
    end
  end
  
  def update
    @edits.each_index do |i|
      if @edits[i].value
        $item_id = i
        GUI::System.Clear
        
        if i == @edits.length-1
          $item = ItemBase.new
        else
          $item = ItemBase.get(i)
        end
        return $state = ItemEdit.new
      end
    end
  end
  
  def draw
  end
end