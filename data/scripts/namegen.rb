class Generator
  CONSONANTS = "bcdfghjklmnpqrstvwxz"
  VOWELS = "aeiouy"
  
  CONFIG = {min_c: 4, max_c: 6, max_v_row: 2, max_c_row: 2, v_chance: 25}
  PROPABILITIES = {'a' => 8.167, 'b' => 1.492, 'c' => 2.782, 'd' => 4.253, 'e' => 12.702, 'f' => 2.228, 'g' => 2.015, 'h' => 6.094, 'i' => 6.966, 'j' => 0.153, 'k' => 0.772, 'l' => 4.025, 'm' => 2.406, 'n' => 6.749, 'o' => 7.507, 'p' => 1.929, 'q' => 0.095, 'r' => 5.987, 's' => 6.327, 't' => 9.056, 'u' => 2.758, 'v' => 0.978, 'w' => 2.360, 'x' => 0.150, 'y' => 1.974, 'z' => 0.074}
  
  def self.generate_name(name)
    if name.start_with?("%")
      if name[1] == "r"
        name = ""
        length = CONFIG[:min_c] + $world.rand(CONFIG[:max_c] - CONFIG[:min_c] + 1)
        length.times{name << pick_random_with_chances(PROPABILITIES.select{|l| CONSONANTS.include?(l)})}

        i = 0
        cons = 0
        vow = 0
        while i < name.length
          if cons == CONFIG[:max_c_row] or vow < CONFIG[:max_v_row] && $world.rand(101) <= CONFIG[:v_chance]
            name.insert(i, pick_random_with_chances(PROPABILITIES.select{|l| VOWELS.include?(l)}))
            vow += 1
            cons = 0
          else
            cons += 1
            vow = 0
          end
          i += 1
        end
        
        name.capitalize
      end
    else
      name
    end
  end
end