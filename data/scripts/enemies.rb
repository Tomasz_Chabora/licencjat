class Enemy < Entity
  attr_reader :hp, :max_hp, :attack, :defense, :hard_hit
  
  def initialize
    @state = :idle
    init :enemy
  end
  
  def update
    return on_dead if @dead and @dead+=1
    @collider ||= BoxCollider.new(left, top, right - left, bottom - top)
    
    if @hp > 0
      ai
      
      if !@harmless and @collider.collides?(pl.collider) and pl.damage(self)
        on_attack
      end
      
      if @invincible.class == Fixnum
        @invincible -= 1
        @invincible = nil if @invincible == 0
      end
    else
      on_death
      @dead = 0
      return
    end
    
    @collider.move(left, top)
  end
  
  def pl
    $state.player
  end
  
  def top
    @y
  end
  
  def bottom
    @y + 99
  end
  
  def left
    @x
  end
  
  def right
    @x + 100
  end
  
  def center_x
    (left + right)/2
  end
  
  def center_y
    (top + bottom)/2
  end
  
  def radar(range_left, range_up, range_right = range_left, range_down = range_up)
    pl.right > left - range_left and pl.left < right + range_right and pl.bottom > top - range_up and pl.top < bottom + range_down
  end
  
  def out_of_map
    right < 0 or left > $state.map.width or top > $state.map.height or bottom < 0
  end
  
  def try_damage(collider)
    return if @invincible
    
    if collider.collides?(@collider)
      if @hp != Float::INFINITY
        damage = 8
        @hp -= damage
        @invincible = 15
        PopUp.new(center_x, top - 24, :text, font: fnt("System/Numbers", 12, 16), text: damage)
        Label.new(:enemy, text: @name, enemy: self)
      end
      
      on_damage
    end
  end
  
  def on_dead
    remove
  end
  
  def ai;end
  def on_death;end
  def on_damage;end
  def on_attack;end
  
  @@enemies = {}
  def self.bind_enemy(symbol, type, *modifiers)
    @@enemies[symbol] = [type, modifiers]
  end
  
  def self.get_enemy(symbol)
    @@enemies[symbol].first
  end
  
  def self.has_modifier?(symbol, name)
    @@enemies[symbol].last.find{|mod| mod.start_with?(name)}
  end
end

class Projectile < Enemy #klasa skopiowana z innej mojej gry
  Z=2
  
  def initialize(x,y,type,img,args={})
    @x, @y, @type, @img, @args = x, y, type, img, args
    if @args[:animation]
      @animation=@args[:animation]+[0,0] #[frames, time per frame]
    end
    
    if @args[:enemy]
      @hard_hit = @args[:hard_hit]
      @hp = Float::INFINITY
      @attack = 6
      super()
    else
      init
    end
    
    img = (@img.class == Array ? tls(@img[0], @img[1], @img[2])[@img[3]] : img(@img))
    @collider = BoxCollider.new(@x, @y, img.width, img.height)
    
    @args[:angle]||=0 if @args[:rotate]
  end
  
  def update
    case @type
      when :trail
      @x+=@args[:movex] if @args[:movex]
      @y+=@args[:movey] if @args[:movey]
      
      if !@animation
        if @args[:repeat] and @args[:repeat]>0
          @args[:repeat]-=1
        else
          remove
        end
      end
      
      when :bullet
      @x+=offset_x(@args[:dir],@args[:speed] ? @args[:speed] : 1)
      @y+=offset_y(@args[:dir],@args[:speed] ? @args[:speed] : 1)
      
      if @args[:follow] #[target entity, entity offset x, entity offset y, bullet offset x, bullet offset y]
        x=@args[:follow][0].x+(@args[:follow][1] ? @args[:follow][1] : 0)
        y=@args[:follow][0].y+(@args[:follow][2] ? @args[:follow][2] : 0)
        angle=angle(@x+(@args[:follow][3] ? @args[:follow][3] : 0),@y+(@args[:follow][4] ? @args[:follow][4] : 0),x,y)
        acc=(@args[:accuracy] ? @args[:accuracy] : 1)
        diff=angle_diff(angle,@args[:dir])
        @args[:dir]+=acc if diff.round<acc
        @args[:dir]-=acc if diff.round>acc
        @args[:follow]=nil if @args[:limit] && (@args[:limit]-=1)==0 or @args[:follow][0].removed
        @args[:angle]=@args[:dir]+@args[:pointing] if @args[:pointing]
      end
      
      when :arrow
      if !@args[:vx] or !@args[:vy]
        @args[:vx]||=offset_x(@args[:dir],@args[:power])
        @args[:vy]||=offset_y(@args[:dir],@args[:power])
      end
      
      @x+=@args[:vx]
      @y+=@args[:vy]
      @args[:vy]+=(@args[:gravity] ? @args[:gravity] : 1)
      @args[:angle]=angle(0,0,@args[:vx],@args[:vy])+@args[:pointing] if @args[:pointing]
      
      remove if @y > $state.map.height
    end
    
    @args[:angle]+=@args[:rotate] if @args[:rotate]
    
    @collider.move(@x, @y)
    
    super if @args[:enemy]
  end
  
  def draw
    if @animation
      @animation[2]+=1
      if @animation[2]==@animation[1]
        @animation[2]=0
        @animation[3]+=1
        if @animation[3]==@animation[0]
          @animation[3]=0
          
          if @type==:trail
            if @args[:repeat] and @args[:repeat]>0
              @args[:repeat]-=1
            else
              remove
            end
          end
        end
        
        if @args[:sequence]
          @img[3]=@args[:sequence][@animation[3]]
        else
          @img[3]=@animation[3]
        end
      end
    end
    
    img=(@img.class == Array ? tls(@img[0], @img[1], @img[2])[@img[3]] : img(@img))
    size=[img.width,img.height]
    if @args[:angle]
      img.draw_rot(@x+size[0]/2,@y+size[1]/2,@args[:z] ? @args[:z] : Z,@args[:angle],0.5,0.5,@args[:scalex] ? @args[:scalex] : 1,@args[:scaley] ? @args[:scaley] : 1,@args[:color] ? @args[:color] : 0xffffffff)
    else
      img.draw(@x,@y,@args[:z] ? @args[:z] : Z,@args[:scalex] ? @args[:scalex] : 1,@args[:scaley] ? @args[:scaley] : 1,@args[:color] ? @args[:color] : 0xffffffff)
    end
  end
  
  def bottom
    @y + @collider.w
  end
  
  def right
    @x + @collider.h
  end
  
  def on_damage
    eval @args[:on_damage] if @args[:on_damage]
  end
end

class Skeleton < Enemy
  def initialize(x, y)
    super()
    @x = (x+0.5) * Tile::SIZE - 50
    @y = (y+1) * Tile::SIZE - 100
    @dir = [:left, :right][rand(2)]
    
    @name = "Skeleton"
    @hp = @max_hp = 20
    @attack = 8
    @defense = 0
  end
  
  def ai
    case @state
      when :idle
      @dir = (@dir == :left ? :right : :left) if $state.time%60 == 0 and rand(3) == 0
      
      @walk = false
      @state = :follow if radar(600, 100)
      
      when :follow
      @walk = false
      if pl.center_x > center_x and !$state.solid?(right+1, bottom-1)
        @walk = true
        @dir = :right
        @x += 1
      elsif pl.center_x < center_x and !$state.solid?(left-1, bottom-1)
        @walk = true
        @dir = :left
        @x -= 1
      end
      
      if radar(200, 400)
        @state = :attack
      elsif !radar(600, 400)
        @state = :idle
      end
      
      when :attack
      @walk = false
      @_attack ||= 0
      @_attack += 1
      
      if @_attack < 0
        @dir = :right if pl.center_x > center_x
        @dir = :left if pl.center_x < center_x
      elsif @_attack == 27
        snd("SkeletonThrow").play
        Projectile.new(center_x - 15, @y, :arrow, "Enemies/SkeletonBone", vx: vx=(@dir == :right ? 4 : -4), vy: -20, rotate: vx*4, enemy: true, hard_hit: true,
        on_damage: "return if @damaged; @damaged = true; @args[:rotate] = @args[:angle] = @args[:vx] = @harmless = 0; @args[:vy] = 4; snd('Bone').play")
      elsif @_attack == 36
        if !radar(200, 400)
          @_attack = nil
          @state = :follow
        else
          @_attack = -60
        end
      end
    end
  end
  
  def draw
    tls("Enemies/Skeleton", 100)[animation_frame].draw(@x + (@dir==:right ? 100 : 0), @y, 2, @dir==:right ? -1 : 1)
  end
  
  def animation_frame
    if @_attack and @_attack > 0
      ([4] * 3 + [5, 6, 7])[@_attack/6]
    elsif @walk
      $state.time/8%4
    else
      0
    end
  end
  
  def top
    @y + 4
  end
  
  def left
    @x + 26
  end
  
  def right
    @x + 76
  end
  
  def on_damage
    snd("SkeletonDamage").play
  end
  
  def on_death
    snd("SkeletonCollapse").play
    Particle.new(@x + (@dir == :right ? 50 : 26), @y, "Enemies/SkeletonSkull", z: Tile::Z, rotate: -3 + rand(6), vy: -2 - rand(3), mirror_x: (@dir == :right))
    5.times{Particle.new(@x + 32 + rand(32), @y + 21 + rand(30), "Enemies/SkeletonBone", z: Tile::Z, angle: rand(360), rotate: -5 + rand(11), vx: -2 + rand(5), vy: -3 - rand(5))}
  end
end

class Spider < Enemy
  def initialize(x, y)
    super()
    @x = x * Tile::SIZE
    @y = y * Tile::SIZE - 17
    @y0 = @y
    
    @name = "Spider"
    @hp = @max_hp = 10
    @attack = 5
    @defense = 0
  end
  
  def ai
    case @state
      when :idle
      if radar(100, 50, 100, 600)
        snd("SpiderFall").play
        @state = :fall
        @hard_hit = true
      end
      
      when :fall
      @y += 16
      
      if @y+80 > $state.map.height or $state.solid?(@x, @y + 80) or $state.solid?(@x + 39, @y + 80)
        @state = :hang
        @hard_hit = false
        @hanging = -30
        @y1 = @y
      end
      
      when :hang
      @hanging += 1
      if @hanging > 0
        @y = @y1 - offset_x(@hanging * 2, 40).to_i.abs
      end
    end
  end
  
  def draw
    if @state != :idle
      dy = ((@y+10-@y0)/24)
      dy.times do |y|
        img("Enemies/SpiderLine").draw(@x + 18, @y0 + y*24, 1)
      end
      img("Enemies/SpiderLine").subimage(0, 0, 4, @y+10-@y0 - dy*24).draw(@x + 18, @y0 + dy*24, 1) if dy*24 != (@y+10-@y0)
    end
    
    tls("Enemies/Spider", 40, 47)[@state == :idle ? 0 : 1].draw(@x, @y, 1) if !@dead
  end
  
  def top
    @y
  end
  
  def bottom
    @y + 46
  end
  
  def left
    @x
  end
  
  def right
    @x + 39
  end
  
  def on_damage
    snd("SpiderDamage").play
  end
  
  def on_death
    snd("SpiderDie").play
    vx = -2 + rand(5)
    Particle.new(@x, @y, ["Enemies/Spider", 40, 47, 1], vx: vx, rotate: vx*2, z: Tile::Z)
  end
  
  def on_dead
    @y -= 8
    remove if @y <= @y0
  end
end

class Mummy < Enemy
  def initialize(x, y)
    super()
    @x = (x+0.5) * Tile::SIZE - 25
    @y = (y+1) * Tile::SIZE - 100
    @dir = [:left, :right][rand(2)]
    
    @name = "Mummy"
    @hp = @max_hp = 30
    @attack = 10
    @defense = 0
  end
  
  def ai
    case @state
      when :idle
      if !$state.solid?(@dir==:right ? right+1 : left-1, top+1) and !$state.solid?(@dir==:right ? right+1 : left-1, (top+bottom)/2) and !$state.solid?(@dir==:right ? right+1 : left-1, bottom-2) and $state.solid?(@dir==:right ? right+1 : left-1, bottom+40, true)
        @x += (@dir == :left ? -1 : 1)
        2.times{|y| @y-=1 if $state.solid?(@dir==:right ? right : left, bottom)}
        2.times{|y| @y+=1 if !$state.solid?(@dir==:right ? left : right, bottom+1) and !$state.solid?(@dir==:right ? right : left, bottom+1) and $state.solid?(@dir==:right ? left : right, bottom+3)}
      else
        @dir = (@dir == :left ? :right : :left)
      end
      
      if $state.time%120 == 0 and rand(2) == 0 and radar(100, 200)
        snd("MummyIdle").play
      end
    end
    
    remove if out_of_map
  end
  
  def draw
    if @dead
      tls("Enemies/MummyDie", 45, 87)[@dead/4].draw(@x + (@dir==:right ? 43 : 2), @y + 13, 2, @dir==:right ? -1 : 1)
    else
      tls("Enemies/Mummy", 50, 100)[$state.time/8%6].draw(@x + (@dir==:right ? 50 : 0), @y, 2, @dir==:right ? -1 : 1)
    end
  end
  
  def top
    @y + 13
  end
  
  def left
    @x + 9
  end
  
  def right
    @x + 45
  end
  
  def on_damage
    snd("MummyDamage").play
  end
  
  def on_death
    snd("MummyDie").play
  end
  
  def on_dead
    remove if @dead == 32
  end
end

class Thornweed < Enemy
  def initialize(x, y)
    super()
    @x = (x+0.5) * Tile::SIZE - 40
    @y = (y-1) * Tile::SIZE
    
    @name = "Thornweed"
    @hp = @max_hp = 8
    @attack = 10
    @defense = 0
    @frame = 0
  end
  
  def ai
    case @state
      when :idle
      if radar(200, 400)
        snd("ThornweedGrowth").play
        @state = :sprout
        @time = 0
      end
      
      when :sprout
      @time += 1
      @frame = @time/8
      @state = :awake if @time == 23
      
      when :awake
      @frame = [3, 4, 3 ,5][$state.time/8 % 4]
      
      if rand(60) == 0 and radar(100, 200)
        snd("ThornweedAttack").play
      end
    end
  end
  
  def draw
    if @dead
      tls("Enemies/ThornweedDie", 52, 57)[@dead/4].draw(@x + 14, @y + 23, 2)
    else
      tls("Enemies/Thornweed", 80, 80)[@frame].draw(@x, @y, 2)
    end
  end
  
  def top
    @y + 32
  end
  
  def bottom
    @y + 80
  end
  
  def left
    @x + 16
  end
  
  def right
    @x + 64
  end
  
  def on_damage
    snd("ThornweedDamage").play
  end
  
  def on_death
    snd("ThornweedDie").play
  end
  
  def on_attack
    snd("ThornweedAttack").play
  end
  
  def on_dead
    remove if @dead == 28
  end
end

class Enemy
  bind_enemy(:skeleton, Skeleton, "on ground")
  bind_enemy(:spider, Spider, "on ceiling")
  bind_enemy(:mummy, Mummy, "on ground")
  bind_enemy(:thornweed, Thornweed, "on ground")
end