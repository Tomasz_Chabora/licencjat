class LocationBase
  attr_writer :areas, :replacements, :names, :color, :music
  
  def self.refresh
    @@locations = []
    (Dir.entries("data/base/Locations") - ['.','..']).each do |id|
      @@locations << Marshal.load(f = File.new("data/base/Locations/#{id}"))
      f.close
    end
  end
end

class LocationEdit
  def initialize
    @return = GUI::Button.new(720, 4, "Return")
    @save = GUI::Button.new(4, 4, "Save")
    GUI::Label.new(48, 4, "Location " + $location_id.to_s.rjust(4, "0"))
    @name = GUI::Textbox.new(192, 4, 32)
    @name.value = Metadata.load(:location_name, $location_id)
    
    GUI::Label.new(8, 32, "Names")
    @names = GUI::Textbox.new(8, 64, 64)
    @names.value = $location.names.join("; ")
    
    GUI::Label.new(8, 96, "Replacements")
    @add_rep = GUI::Button.new(8, 128, "+")
    @delete_rep = GUI::Button.new(48, 128, "-")
    
    GUI::Label.new(500, 96, "Color")
    @colors = [GUI::Spinner.new(500, 128, 0, 255), GUI::Spinner.new(580, 128, 0, 255), GUI::Spinner.new(660, 128, 0, 255)]
    @colors.each.with_index{|gui, i| gui.value = $location.color[i]}
    
    GUI::Label.new(500, 160, "Music")
    @music = GUI::Textbox.new(500, 184, 32)
    @music.value = $location.music
    
    @rep_edits = []
    $location.replacements.each.with_index do |rep, i|
      @rep_edits << new_rep_edit(i)
      @rep_edits.last[0].value = rep[:key]
      @rep_edits.last[1].value = rep[:strings].join("; ")
    end
    
    GUI::Label.new(4, 224, "Areas")
    @checks = []
    y0 = 256
    AreaBase.count.times do |i|
      x = 4
      y = i*32
      
      @checks << GUI::Checkbox.new(x, y0+y)
      @checks.last.value = true if $location.areas.include?(i)
      GUI::Label.new(48+x, y0+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:area_name, i)}> Rooms: " + AreaBase.get(i).rooms.join(", "))
    end
  end
  
  def new_rep_edit(i)
    [GUI::Textbox.new(80, 128 + i*24, 4),
        GUI::Textbox.new(140, 128 + i*24, 32)]
  end
  
  def update
    if @add_rep.value
      @rep_edits << new_rep_edit(@rep_edits.length)
     end
      
    if @delete_rep.value and !@rep_edits.empty?
      @rep_edits.last.each{|e| e.remove}
      @rep_edits.pop
    end
    
    if @return.value
      GUI::System.Clear
      LocationBase.refresh
      
      return $state = LocationBrowser.new
    end
    
    save if @save.value
  end
  
  def draw
  end
  
  def save
    Metadata.store(:location_name, $location_id, @name.value)
    $location.areas = @checks.each_index.to_a.select{|v| @checks[v].value}
    $location.names = @names.value.split(/;\s*/)
    $location.replacements = @rep_edits.collect{|r| {:key => r[0].value, :strings => r[1].value.split(/;\s*/)}}
    $location.color = @colors.collect{|r| r.value}
    $location.music = @music.value
    
    f = File.new("data/base/Locations/" + $location_id.to_s.rjust(4, "0"), 'w')
    Marshal.dump($location, f)
    f.close
  end
end

class LocationBrowser
  def initialize
    @rooms = []
    @edits = []
    
    (LocationBase.count+1).times do |i|
      x = 4
      y = i*32
      
      if i == LocationBase.count
        y = i*32
        @edits << GUI::Button.new(4+x, 4+y, "New")
      else
        @edits << GUI::Button.new(x, 4+y, "Edit")
        GUI::Label.new(48+x, 4+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:location_name, i)}> Areas: " + LocationBase.get(i).areas.join(", "))
      end
    end
  end
  
  def update
    @edits.each_index do |i|
      if @edits[i].value
        $location_id = i
        GUI::System.Clear
        
        if i == @edits.length-1
          $location = LocationBase.new
        else
          $location = LocationBase.get(i)
        end
        return $state = LocationEdit.new
      end
    end
  end
  
  def draw
  end
end