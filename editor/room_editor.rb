class RoomBase
  attr_accessor :name, :modifiers
  attr_writer :min_width, :min_height, :max_width, :max_height, :segments
  attr_writer :vertical_exit, :horizontal_exit, :background
  
  def self.refresh
    @@rooms = []
    (Dir.entries("data/base/Rooms") - ['.','..']).each do |id|
      @@rooms << Marshal.load(f = File.new("data/base/Rooms/#{id}"))
      f.close
    end
  end
end

class RoomEdit
  def initialize
    @return = GUI::Button.new(720, 4, "Return")
    @save = GUI::Button.new(4, 4, "Save")
    GUI::Label.new(48, 4, "Room " + $room_id.to_s.rjust(4, "0"))
    @name = GUI::Textbox.new(192, 4, 32)
    @name.value = Metadata.load(:room_name, $room_id)
    
    GUI::Label.new(4, 224, "Segments")
    @segments = []
    @checks = []
    @labels = []
    y0 = 256
    SegmentBase.count.times do |i|
      x = i%3*260
      y = i/3*224
      
      @checks << GUI::Checkbox.new(4+x, y0+y)
      @checks.last.value = true if $room.segments.include?(i)
      
      @labels << GUI::Label.new(24+x, y0+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:segment_name, i)}>")
      @segments << SegmentPreview.new(SegmentBase.get(i).grid, 4+x, y0+28+y, true)
    end
    
    GUI::Label.new(4, 40, "Min. width")
    @min_w = GUI::Spinner.new(4, 64, 1, 100)
    @min_w.value = $room.min_width
    
    GUI::Label.new(128, 40, "Max. width")
    @max_w = GUI::Spinner.new(128, 64, 1, 100)
    @max_w.value = $room.max_width
    
    GUI::Label.new(252, 40, "Min. height")
    @min_h = GUI::Spinner.new(252, 64, 1, 100)
    @min_h.value = $room.min_height
    
    GUI::Label.new(376, 40, "Max. height")
    @max_h = GUI::Spinner.new(376, 64, 1, 100)
    @max_h.value = $room.max_height
    
    GUI::Label.new(4, 88, "Vert. chance")
    @v_chance = GUI::Spinner.new(4, 112, 0, 1000)
    @v_chance.value = $room.vertical_exit
    
    GUI::Label.new(128, 88, "Hor. chance")
    @h_chance = GUI::Spinner.new(128, 112, 0, 1000)
    @h_chance.value = $room.horizontal_exit
    
    GUI::Label.new(252, 88, "Background")
    @background = GUI::List.new(252, 112, 3, *["None"] + Array.new(BackgroundBase.count) {|i| Metadata.load(:background_name, i)})
    @background.value = $room.background+1
    
    GUI::Label.new(4, 148, "Modifiers")
    @modifiers = GUI::Textbox.new(4, 172, 64)
    @modifiers.value = $room.modifiers.join("; ")
    
    @up = GUI::Button.new(780, 256, "▲")
    @down = GUI::Button.new(780, 576, "▼")
    @scroll = 0
    @max_scroll = (@segments.length-3)/3
    move_segments
  end
  
  def update
    if @up.value or $screen.mouse_y > 256 && key_press(MsWheelUp)
      @scroll -= 1 if @scroll > 0
      move_segments
    end
    
    if @down.value or $screen.mouse_y > 256 && key_press(MsWheelDown)
      @scroll += 1 if @scroll < @max_scroll
      move_segments
    end
    
    if @return.value
      GUI::System.Clear
      RoomBase.refresh
      
      return $state=RoomBrowser.new
    end
    
    save if @save.value
  end
  
  def draw
    @segments.each{|s| s.draw}
    
    h = 4/(@max_scroll+4).to_f * 296
    y = 280 + 296.0/(@max_scroll+4) * @scroll
    draw_quad(780, y, Color::WHITE, 800, y, Color::WHITE, 800, y+h, Color::WHITE, 780, y+h, Color::WHITE, 0)
  end
  
  def save
    Metadata.store(:room_name, $room_id, @name.value)
    $room.min_width = @min_w.value
    $room.min_height = @min_h.value
    $room.max_width = @max_w.value
    $room.max_height = @max_h.value
    $room.vertical_exit = @v_chance.value
    $room.horizontal_exit = @h_chance.value
    $room.background = @background.value-1
    $room.modifiers = @modifiers.value.split(/;\s*/)
    
    $room.segments = @checks.each_index.to_a.select{|v| @checks[v].value}
    
    f = File.new("data/base/Rooms/" + $room_id.to_s.rjust(4, "0"), 'w')
    Marshal.dump($room, f)
    f.close
  end
  
  def move_segments
    @checks.each.with_index do |check, i|
      check.y = (i/3-@scroll)*128+256
      check.disabled = (![1,2].include?(check.y/224))
    end
    @labels.each.with_index do |label, i|
      label.y = (i/3-@scroll)*128+256
      label.disabled = (![1,2].include?(label.y/224))
    end
    @segments.each.with_index do |segment, i|
      segment.y = (i/3-@scroll)*128+284
      segment.disabled = (![1,2].include?(segment.y/224))
    end
  end
end

class RoomBrowser
  def initialize
    @rooms = []
    @edits = []
    
    (RoomBase.count+1).times do |i|
      x = 4
      y = i*32
      
      if i == RoomBase.count
        y = i*32
        @edits << GUI::Button.new(4+x, 4+y, "New")
      else
        @edits << GUI::Button.new(x, 4+y, "Edit")
        GUI::Label.new(48+x, 4+y, i.to_s.rjust(4, "0") + " <#{Metadata.load(:room_name, i)}> Segments: " + RoomBase.get(i).segments.join(", "))
      end
    end
  end
  
  def update
    @edits.each_index do |i|
      if @edits[i].value
        $room_id = i
        GUI::System.Clear
        
        if i == @edits.length-1
          $room = RoomBase.new
        else
          $room = RoomBase.get(i)
        end
        return $state=RoomEdit.new
      end
    end
  end
  
  def draw
  end
end