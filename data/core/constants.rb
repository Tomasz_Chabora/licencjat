SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

U = 0
R = 1
D = 2
L = 3

OPPOSITE = {U => D, R => L, D => U, L => R}

C_ROOM = 0
C_EXIT = 1
C_BOTH = 2

HUD_Z = 50

class Tile
  SIZE = 40
  SIZE2 = SIZE*2
  SIZE_2 = SIZE/2
  Z = 30
end

class SegmentBase
  WIDTH = SCREEN_WIDTH / Tile::SIZE
  HEIGHT = SCREEN_HEIGHT / Tile::SIZE
end