#POWERDED BY SAPPHIRE V.1.11

begin
require 'gosu'
include Gosu

scripts = Dir.entries('data/core') - ['.','..']
scripts.each{|scr| require_relative 'data/core/' + scr}

scripts = Dir.entries('data/scripts') - ['.','..']
scripts.each{|scr| require_relative 'data/scripts/' + scr}

class Main < Window
  def initialize(fullscreen = false)
    super(SCREEN_WIDTH, SCREEN_HEIGHT, fullscreen)
    self.caption = "Infinite Dungeons"
    $time = 0
  end

  def update
    $sapphire_system.update
    $state.update
    $time+=1
  end

  def draw
    $state.draw
    $sapphire_system.draw
  end
  
  def button_down(id)
    $sapphire_system.button_down = id
    $state.button_down(id) if $state.respond_to?(:button_down)
    
    if id == KbF1 ##bardzo bardzo debug
      begin
      eval File.readlines("Hackerman.rb").join("\n")
      rescue Exception => e
      puts e
      puts e.backtrace
      end
    end
  end
  
  def button_up(id)
    $sapphire_system.button_up = id
    $state.button_up(id) if $state.respond_to?(:button_up)
  end
  
  # def needs_cursor?;true end ##debug
end

$screen = Main.new
$state = Load.new
$screen.show

rescue Exception => e
f=File.new('crash log.txt','w')
f.puts e
f.puts e.backtrace
f.puts("World seed: " + $world.random.seed.to_s) if $world
f.close
puts e
puts e.backtrace
end