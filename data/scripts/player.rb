class Player < Entity
  JUMP_HEIGHT = 6
  JUMP_STRENGTH = 16
  MAX_SPEED = 8
  DASH_SPEED = MAX_SPEED * 2
  DASH_TIMEOUT = 15
  SLIDE_SPEED = 20
  WALL_JUMP_STRENGTH = 8
  WALL_JUMP_SPEED = 12
  DOUBLE_JUMP_LOSS = 2
  DOUBLE_JUMP_STRENGTH = 12
  
  Z = 3
  SUPER_JUMP_PARTICLES = 2
  
  def initialize
    @x = 350
    @y = 340
    @vx = @vy = 0
    
    @state = :standing
    @collider = BoxCollider.new(left, top, right - left, bottom - top)
    
    init
  end
  
  def init
    super
    @super_jump.init if @super_jump
  end
  
  def update
    platform = (@vy >= 0 and not key_hold(:down) && key_press(:jump))
    on_ground = false
    $water_walking = ability?(:water_walking)
    
    @vy.to_i.abs.times do
      if !$state.solid?(left+1 ,@vy>0 ? bottom+1 : top-1, platform) and !$state.solid?(right-1, @vy>0 ? bottom+1 : top-1, platform)
        @y += (@vy<=>0)
      else
        snd("Land").play if @vy > 4
        @vy = 0
        @jump2 = nil
        on_ground = true
        break
      end
    end
      
    if @super_jump
      @super_jump.y = @y
      SUPER_JUMP_PARTICLES.times do
        Particle.new(@super_jump.x + 9, @super_jump.y + 52, ["Effects/GenericSparks", 5, 5, rand(5)], vx: -2 + rand(5), color: Color.new(255, rand(256), 0))
      end
      
      if @vy >= 0
        Particle.new(@super_jump.x, @super_jump.y, "Player/FireworkUsed", vx: (@dir == :left ? -2 : 2), vy: -4, rotate: (@dir == :left ? -8 : 8), z: Tile::Z)
        @super_jump.remove
        @super_jump = nil
      end
    end
    
    wall = false
    @vx.to_i.abs.times do
      if !$state.solid?(@vx>0 ? right+1 : left-1, top+1) and !$state.solid?(@vx>0 ? right+1 : left-1, (top+bottom)/2) and !$state.solid?(@vx>0 ? right+1 : left-1, bottom-2)
        @x += (@vx<=>0)
        2.times{|y| @y-=1 if $state.solid?(@vx>0 ? right : left, bottom) and !$state.solid?(@vx>0 ? right : left, top-1)}
        2.times{|y| @y+=1 if !$state.solid?(@vx>0 ? left : right, bottom+1, true) and !$state.solid?(@vx>0 ? right : left, bottom+1, true) and $state.solid?(@vx>0 ? left : right, bottom+3)}
      else
        wall = (@vx <=> 0)
        @vx = 0
        @dash = nil
        break
      end
    end
    
    on_ground = ($state.solid?(left, bottom+1, platform) || $state.solid?(right, bottom+1, platform))
    on_ground2 = on_ground || !platform && ($state.solid?(left, bottom+1, true) || $state.solid?(right, bottom+1, true))
    fall_down = (!!on_ground != !!on_ground2)
    $water_walking = nil
    
    case @state
      when :standing
      max_speed = (@super_jump ? 0 : @dash ? DASH_SPEED : MAX_SPEED)
      if key_hold(:right) and @vx < max_speed
        @dash = nil if @dash == :left
        @dir = :right
        @vx += 1
      elsif key_hold(:left) and @vx > -max_speed
        @dash = nil if @dash == :right
        @dir = :left
        @vx -= 1
      elsif @vx.abs > max_speed or !key_hold(:right) && !key_hold(:left)
        @vx -= (@vx<=>0)
        @dash = nil
      end
    
      if key_press(:right)
        if !@dash_ready
          @dash_ready = [:right, DASH_TIMEOUT] if ability?(:dash)
        elsif @dash_ready[0] == :right and on_ground
          @dash = :right
        end
      elsif key_press(:left)
        if !@dash_ready
          @dash_ready = [:left, DASH_TIMEOUT] if ability?(:dash)
        elsif @dash_ready[0] == :left and on_ground
          @dash = :left
        end
      end
      
      if key_press(:special) and on_ground and @vx == 0 || ((@vx < 0) == (@dir == :left))
        snd("Slide").play
        @vx = (@dir == :left ? 16 : -16)
        l = (@dir == :left)
        
        GenericRepeater.new(@x, bottom-20, 10) do
          @x = (l ? $state.player.right-20 : $state.player.left)
          @y = $state.player.bottom-20
          Trail.new(@x, @y, ["Effects/Dust", 20, 20], animation: 2)
        end
      end
    
      if key_press(:jump)
        if !@jump and on_ground || @water
          snd("Jump").play if !@water
          @jump = 0
        elsif ability?(:double_jump) and !@jump2 and !fall_down
          snd("Repulsion").play
          @jump = DOUBLE_JUMP_LOSS
          @jump2 = true
        end
      end
    
      if wall and @vy > 0 and ability?(:wall_jump) and key_hold([:left, nil, :right][wall + 1]) and !on_ground
        @wallslide = [:left, nil, :right][wall + 1]
        @state = :wallslide
      end
      
      @state = :crouch if key_hold(:down) and on_ground
      
      when :crouch
      @vx = 0
      
      if @recover
        @recover -= 1
        @recover = nil if @recover == 0
        @invincible = 10
      else
        @state = :standing if !key_hold(:down)
      
        if on_ground and key_hold(:up) and key_press(:special) and ability?(:super_jump)
          snd("Firework").play
          @state = :standing
          @vy = -JUMP_STRENGTH * 3
          @jump = JUMP_HEIGHT
          @super_jump = Trail.new(@dir == :left ? left-18 : right, @y, "Player/Firework", animation: Float::INFINITY)
        elsif !@slide and on_ground and key_hold(:down) and key_press(:special) and ability?(:slide)
          snd("Slide").play
          @slide = SLIDE_SPEED
          @state = :slide
          @attack = nil
        
          GenericRepeater.new(@x, bottom-20, 10) do
            @x = (l ? $state.player.right-20 : $state.player.left)
            @y = $state.player.bottom-20
            Trail.new(@x, @y, ["Effects/Dust", 20, 20], animation: 2)
          end
        end
      end
      @state = :standing if !on_ground
      
      if key_hold(:left)
        @dir = :left
      elsif key_hold(:right)
        @dir = :right
      end
      
      when :slide
      @vx = @slide * (@dir == :left ? -1 : 1)
      @slide -= 1
      
      if @slide == 0
        @slide = nil
        @state = :crouch
      end
      
      when :wallslide
      snd("WallSlide").play if $time%5 == 0
      
      if @vx.abs > 1 or !key_hold(@wallslide) or on_ground
        @state = :standing 
      elsif key_press(:jump)
        snd("Jump").play
        @vx = WALL_JUMP_SPEED * (@wallslide ==:right ? -1 : 1)
        @dir = (@wallslide == :right ? :left : :right)
        @jump = 0
        @wall_jump = WALL_JUMP_STRENGTH
        @state = :walljump
      else
        @vx = (@wallslide == :right ? 1 : -1)
      end
      
      when :walljump
      @wall_jump -= 1
      
      if @wall_jump == 0
        @wall_jump = nil
        @state = :standing
      end
      
      when :hurt
      @slide = @dash = @attack = nil
      
      @vx -= (@vx <=> 0) if @water
      
      if on_ground && !@recover or @water && @vx == 0
        if @hurt != :light
          @state = :crouch
          @hurt = nil
        end
        @recover = 10
      end
      
      if @recover
        @recover -= 1
        @invincible = 10
        if @recover == 0
          @recover = @hurt = nil
          @state = :standing
        end
      end
    end
    
    if @invincible.class == Fixnum
      @invincible -= 1
      @invincible = nil if @invincible == 0
    end
    
    if key_hold(:jump) and @jump and @jump < JUMP_HEIGHT
      @vy = -(@jump2 ? DOUBLE_JUMP_STRENGTH : JUMP_STRENGTH)
      @jump += 1
    else
      @jump = nil
    end
    
    if @dash_ready
      @dash_ready[1] -= 1
      @dash_ready = nil if @dash_ready[1] == 0
    end
    
    water_line = top + 2*(center_y - top)/3
    if !@water
      @vy += 1
      if $state.water?(center_x, water_line)
        snd("Splash").play
        y = water_line + 30
        60.times do
          y -=1
          if !$state.water?(center_x, y)
            @water = y
            @vy /= 2
            break
          end
        end
      end
    elsif @water
      if !ability?(:sinking)
        if $state.water?(center_x, water_line - 1)
          @vy -= 1
          @swim = true
        elsif !@jump and @vy < 0
          @vy = 0
          @y = @water - (water_line - @y) + 1
          @swim = true
        end
      else
        @vy += 1
      end
      
      if !$state.water?(center_x, water_line+16)
        snd("Splash").play
        @water = @swim = false
      end
    end
    
    if !@attack and key_press(:attack) and ![:slide].include?(@state)
      @attack = 0
      @sword_collider = BoxCollider.new(0, 0, 0, 0)
      snd("Sword").play
    elsif @attack
      @attack+=1
      if @attack == 15
        @attack = @sword_collider = nil
      else
        dx = [3, 56, 68, 74, 71][@attack/3] if @dir == :right
        dx = [62, 35, -11, -32, -16][@attack/3] if @dir == :left
        w = [40, 18, 48, 65, 50][@attack/3]
        @sword_collider.move(@x + dx, @y + [-33, -48, -32, 11, 37][@attack/3] + (@state == :crouch ? 17 : 0))
        @sword_collider.resize(w, [46, 54, 43, 13, 43][@attack/3])
        $state.get_entities(:enemy).each{|enemy| enemy.try_damage(@sword_collider)}
      end
    end
    
    @collider.move(left, top)
  end
  
  def draw
    frame = if @state == :hurt
      anim_hurt
    elsif @attack
      anim_attack
    elsif @state == :wallslide
      anim_wall_slide
    elsif @state == :standing
      if @super_jump
        anim_super_jump
      elsif @vy < 0
        anim_jump
      elsif @vy > 2
        anim_fall
      elsif @swim
        anim_swim
      elsif @vx == 0
        anim_stand
      else
        anim_run
      end
    elsif @state == :walljump
      anim_jump
    elsif @state == :crouch
      anim_crouch
    elsif @state == :slide
      anim_slide
    end
    
    if @attack
      wx = [41, 59, 75, 82, 77][@attack/3]
      wy = [9, 5, 7, 20, 44][@attack/3] + (@state == :crouch ? 17 : 0)
      wa = [-45, 13, 44, 80, 127][@attack/3]
    end
    
    img("Player/Sword1").draw_rot(@x + (@dir == :left ? 100 - wx : wx), @y + wy, 2, @dir == :left ? -wa : wa, 0.5, 0.875) if @attack
    tls("Player/Player", 100)[frame].draw(@x + (@dir == :left ? 100 : 0), @y, Z, @dir == :left ? -1 : 1)
  end
  
  def left
    @x+34
  end
  
  def center_x
    @x+50
  end
  
  def right
    @x+66
  end
  
  def top
    @y + (@slide ? 80 : 8)
  end
  
  def center_y
    @y + (@slide ? 90 : 50)
  end
  
  def bottom
    @y+99
  end
  
  def center_y_absolute
    @y + 50
  end
  
  def ability?(name)
    $save[:abilities].include? name
  end
  
  def damage(enemy)
    return if @invincible
    @hurt = (enemy.center_x < center_x ? :left : :right)
    @invincible = true
    @state = :hurt
    
    platform = (@vy >= 0 and not key_hold(:down) && key_press(:jump))
    on_ground = ($state.solid?(left, bottom+1, platform) || $state.solid?(right, bottom+1, platform))
    
    if enemy.hard_hit or !on_ground
      snd("HurtHard").play
      @vx = (@hurt == :left ? 1 : -1) * 8
      @vy = -8
    else
      snd("HurtWeak").play
      @vx = 0
      @hurt = :light
    end
    
    damage = enemy.attack
    $save[:hp] -= damage
    PopUp.new(center_x, top - 24, :text, font: fnt("System/Numbers", 12, 16), text: damage)
  end
  
  def anim_stand
    0
  end
  
  def anim_swim
    18
  end
  
  def anim_run
    [0, 1, 2, 1, 0, 3, 4, 3][$state.time/4 % 8]
  end
  
  def anim_crouch
    5
  end
  
  def anim_slide
    6
  end
  
  def anim_wall_slide
    7
  end
  
  def anim_super_jump
    24
  end
  
  def anim_jump
    8
  end
  
  def anim_fall
    9
  end
  
  def anim_attack
    attack = [10, 11, 12, 13, 14][@attack/3] + (@state == :crouch ? 9 : 0)
  end
  
  def anim_hurt
    @hurt == @dir ? 15 : @hurt == :light ? 17 : 16
  end
end