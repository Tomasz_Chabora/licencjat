class System
  attr_reader :keys
  attr_reader :music,:images,:tiles,:sounds,:fonts
  attr_writer :gui
  attr_accessor :premusic,:light_shader,:button_down,:button_up
  def initialize
    @keys={}
    @music={}
    @images={}
    @tiles={}
    @sounds={}
    @fonts={}
  end
  
  def update
    Msc[@premusic[1]].play(true) if @premusic and !@premusic[0].playing? and !@premusic[0].paused?
  end
  
  def draw
    @button_down = @button_up = nil
  end
  
  def key_name(id)
    unless defined? @button_names
      button_constants = Gosu.constants(false).grep(/^(?:Gp|Kb|Ms)/).inject({}) {|h, k| h.merge! Gosu.const_get(k) => k}
      
      @button_names = Hash.new do |names, id|
        names[id] = button_constants[id].to_s.gsub("Kb", "").gsub("Ms", "Mouse ")
      end
    end
    
    @button_names[id]
  end
end
$sapphire_system = System.new

def key_press(id = true)
  raise ArgumentError, "Expecting Symbol or Gosu::Button or true" if !id.is_a?(Symbol) and !id.is_a?(Fixnum) and id != true
  key = (id.class == Symbol ? $sapphire_system.keys[id] : id)
  return $sapphire_system.button_down if $sapphire_system.button_down == key or id == true
end

def key_release(id = true)
  raise ArgumentError, "Expecting Symbol or Gosu::Button or true" if !id.is_a?(Symbol) and !id.is_a?(Fixnum) and id != true
  key = (id.class == Symbol ? $sapphire_system.keys[id] : id)
  return $sapphire_system.button_up if $sapphire_system.button_up == key or id == true
end

def key_hold(id)
  raise ArgumentError, "Expecting Symbol or Gosu::Button" if !id.is_a?(Symbol) and !id.is_a?(Fixnum)
  key = (id.class == Symbol ? $sapphire_system.keys[id] : id)
  $screen.button_down? key
end

def bind_keys(keys={}, keyset = $sapphire_system.keys)
  raise ArgumentError, "Expecting Symbol => Gosu::Button hash" if keys.keys.find{|key| !key.is_a?(Symbol)} or keys.values.find{|value| !value.is_a?(Fixnum)}
  keys.each_pair{|key,value| keyset[key]=value}
end

def key_bound(id, keyset = $sapphire_system.keys)
  raise "Expecting Symbol" if id.class != Symbol
  keyset[id]
end

def key_name(id)
  raise "Expecting Gosu::Button" if id.class != Fixnum
  $sapphire_system.key_name(id)
end

def msc(name)
  if !$sapphire_system.music[name.downcase]
    dir=((Dir.exists?(file="data/music/#{name.split('/')[0]}") or File.exists?(file+'.ogg')) ? '/music' : '')
    $sapphire_system.music[name.downcase] = Song.new("data#{dir}/#{name}.ogg")
  end
  
  $sapphire_system.music[name.downcase]
end

def loop_music(name)
  pre = File.exists?("data/music/.#{name}.ogg")
  
  $sapphire_system.premusic=nil
  if pre
    $sapphire_system.premusic=[msc(name), name]
    name = "." + name
  end
  
  return if msc(name).playing? ##nie będzie działać z custom loopami
  msc(name).play(!pre)
end

def stop_music
  Song.current_song.stop if Song.current_song
  $sapphire_system.premusic=nil
end

def pause_music
  Song.current_song.pause if Song.current_song
end

def resume_music
  Song.current_song.play($sapphire_system.premusic[0] != Song.current_song) if Song.current_song
end

def img(name, options = {})
  if !$sapphire_system.images[name.downcase]
    $sapphire_system.images[name.downcase] = Image.new("data/gfx/#{name}.png", options)
  end
  $sapphire_system.images[name.downcase]
end

def tls(name, width, height = width, options = {})
  if !$sapphire_system.tiles["#{name.downcase}_#{width}_#{height}"]
    $sapphire_system.tiles["#{name.downcase}_#{width}_#{height}"] = Image.load_tiles(img(name), width, height, options)
  end
  $sapphire_system.tiles["#{name.downcase}_#{width}_#{height}"]
end

def snd(name)
  if !$sapphire_system.sounds[name.downcase]
    $sapphire_system.sounds[name.downcase] = Sample.new("data/sfx/#{name}.ogg")
  end
  $sapphire_system.sounds[name.downcase]
end

def fnt(name, size, size2 = nil)
  if size2
    if !$sapphire_system.fonts["#{name.downcase}_#{size}_#{size2}"]
      $sapphire_system.fonts["#{name.downcase}_#{size}_#{size2}"] = BitmapFont.new(name, size, size2)
    end
    $sapphire_system.fonts["#{name.downcase}_#{size}_#{size2}"]
  else
    if !$sapphire_system.fonts["#{name.downcase}_#{size}"]
      $sapphire_system.fonts["#{name.downcase}_#{size}"] = Font.new($screen, name, size)
    end
    $sapphire_system.fonts["#{name.downcase}_#{size}"]
  end
end

class BitmapFont
  DFACTOR = 1
	def initialize(*images)
		@images = images
	end
  
  def define_characters(*characters)
    @characters = characters
  end

	def draw(text,x,y,z,args={})
		posx=posy=0
		scalex=(args[:scalex] ? args[:scalex] : 1)
		scaley=(args[:scaley] ? args[:scaley] : 1)
		xspacing=(args[:xspacing] ? args[:xspacing] : @images[1]*scalex)
		yspacing=(args[:yspacing] ? args[:yspacing] : @images[1]*scaley)
		max=args[:max]
		align=args[:align]
		
    text=text.to_s
		text.each_char{|char| index=@characters.index(char.upcase)
    scalex1=(char.upcase==char ? scalex : scalex*DFACTOR)
    scaley1=(char.upcase==char ? scaley : scaley*DFACTOR)
		tls(*@images)[index].draw(x+posx-(align==:right ? (text.length*xspacing) : align==:center ? (text.length*xspacing)/2 : 0),y+posy+(scaley1 != scaley ? yspacing*(1-DFACTOR) : 1),z,scalex1,scaley1,args[:color] ? args[:color] : 0xffffffff) if index
		posx+=xspacing
		(posy+=yspacing ; posx=0) if char=="\n" or max && posx+xspacing>max}
	end
end

class Entity
	attr_accessor :x,:y,:_no_update,:_no_draw
  attr_reader :_removed,:_spawn_time,:_tags, :collider
  
	def init(*tags)
    @_tags = tags
    @_spawn_time = $time
    @_removed = false
    
		$state.add(self)
	end

	def remove
    # @collider.remove if @collider ##nie trzeba (jeszcze)
    $state.remove(self)
    @_removed = true
	end

	def gravity(width, height = width, gravity = 1)
		@vy ||= 0
		@vy += gravity
		if @vy>0
			@vy.to_i.times{if !$state.solid?(@x,@y+height,true) and !$state.solid?(@x+width,@y+height,true) ; @y+=1 else @vy=0 end}
		elsif @vy<0
			(-@vy).to_i.times{if !$state.solid?(@x,@y,true) and !$state.solid?(@x+width,@y,true) ; @y-=1 else @vy=0 end}
		end
	end
    
  def life_time
    $time - @_spawn_time
  end
end

class State
  attr_reader :entities
  
  FLASH_Z=5
  SHAKE_Z=5
  def groups; [] end
  
  def initialize(*args)
		$state=self
		reset
    
    @screen_x = @screen_y = 0
    setup(*args)
  end
  
  def update
    pre
    
		@entities.delete_if do |ent|
      ent.update if ent.respond_to?(:update) and !ent._no_update
      ent._removed
    end if !@no_update
    
    post
  end
  
  def draw
    back
    
    $screen.translate(-@screen_x, -@screen_y) do
      @entities.each {|ent| ent.draw if ent.respond_to?(:draw) and !ent._no_draw}
    end
    
    $screen.flush
    front
  end

	def solid?(x, y, down=true)
	end

	def reset
		@entities=[]
    @groups=[[]]*groups.length
    @camera.init if @camera
	end
  
  def find(group = 0)
    @entities.find{|ent| yield(ent)}
  end
  
  def get_entities(*tags)
    if tags.empty?
      @entities
    else
      @groups.select.with_index{|whatevs, i| !(groups[i] & tags).empty?}.flatten
    end
  end
  
  def add(entity)
    @entities << entity
    @groups.each_index{|i|
      @groups[i] << entity if !(groups[i] & entity._tags).empty?
    }
  end
  
  def remove(entity)
    @groups.each{|group| group.delete(entity)}
  end
  
  def setup; end
  def pre; end
  def post; end
  def back; end
  def front; end
end
