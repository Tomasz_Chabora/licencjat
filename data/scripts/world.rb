class World
  attr_reader :locations, :areas, :rooms, :exits, :square_count, :random, :items, :obstacles, :save_points
  
  def initialize(seed)
    @seed = seed
    @random = Random.new(seed)
    @locations = []
    @areas = []
    @area_marks = {}
    @rooms = []
    @exits = []
    @save_points = []
    @obstacles = {}
    @prev_exits = []
    @square_count = @exit_blocker = 0
    
    @items = []
    @keys = []
    @paths = []
  end
  
  def validate_room(room)
    room.w.times do |dx|
      room.h.times do |dy|
        return if get_room(room.x + dx, room.y + dy, room) or get_area(room.x + dx, room.y + dy) != room.area
      end
    end
    true
  end
  
  def get_room(x, y, ignore = nil)
    @rooms.find do |room| next if room == ignore
      room.x <= x and room.x + room.w > x and room.y <= y and room.y + room.h > y
    end
  end
  
  def get_area(x, y)
    @areas.find do |area|
      area.x <= x and area.x + area.w > x and area.y <= y and area.y + area.h > y
    end
  end
  
  def rand(max, min=0)
    return max if max == min
    @random.rand(min...max)
  end
  
  def reset_random(variation)
    @random = Random.new(@seed * variation)
  end
  
  def count_squares_and_clean
    remove_instance_variable(:@prev_exits)
    remove_instance_variable(:@exits)
    remove_instance_variable(:@accessibility)
    
    @square_count = 0
    @rooms.each do |room|
      room.clear_cache
      @square_count += room.w * room.h
    end
  end
  
  def validate_area(area, direction)
    return if area.base.has_modifier?("unique y") and @areas.find{|a| a.type == area.type and a.y == area.y}
    
    if mod = area.base.has_modifier?("align left") and @area_marks[mod.split.last]
      mark = @area_marks[mod.split.last]
      mark = mark.x + mark.w
      
      return if area.x + area.w < mark
      
      if area.x < mark
        dx = mark - area.x
        return if area.w - dx < area.base.min_width or direction == R
        
        area.x += dx
        area.w -= dx
      end
    end
    if mod = area.base.has_modifier?("align right") and @area_marks[mod.split.last]
      mark = @area_marks[mod.split.last]
      mark = mark.x + mark.w
      
      return if area.x > mark
      
      if area.x + area.w > mark
        dx = area.x + area.w - mark
        return if area.w - dx < area.base.min_width or direction == L
        
        area.w -= dx
      end
    end
    
    true
  end
  
  def area_intersect(area)
    @areas.each{|a| if area.x + area.w > a.x and area.y + area.h > a.y and
      area.x < a.x + a.w and area.y < a.y + a.h then return true end}
    false
  end
  
  def identify_area_neighbours(area)
    neighbours = []
    
    area.w.times do |dx|
      room = get_room(area.x + dx, area.y - 1)
      neighbours << [room, D] if room
      room = get_room(area.x + dx, area.y + area.h)
      neighbours << [room, U] if room
    end if area.base.rooms.find{|r| !RoomBase.get(r).has_modifier?("restrict hor")}
    
    area.h.times do |dy|
      room = get_room(area.x - 1, area.y + dy)
      neighbours << [room, R] if room
      room = get_room(area.x + area.w, area.y + dy)
      neighbours << [room, L] if room
    end
    
    neighbours.uniq
  end
  
  def identify_area_neighbour_areas(area)
    neighbours = []
    
    area.w.times do |dx|
      area2 = get_area(area.x + dx, area.y - 1)
      neighbours << area2 if area2 and !area2.empty
      area2 = get_area(area.x + dx, area.y + area.h)
      neighbours << area2 if area2 and !area2.empty
    end
    
    area.h.times do |dy|
      area2 = get_area(area.x - 1, area.y + dy)
      neighbours << area2 if area2 and !area2.empty
      area2 = get_area(area.x + area.w, area.y + dy)
      neighbours << area2 if area2 and !area2.empty
    end
    
    neighbours.uniq
  end
  
  def update_accessibility
    @accessibility ||= {}
    
    @save_points.each do |save|
      queue = [{x: save.x, y: save.y, back: []}]
      visited = []
      
      while !queue.empty?
        visited << (cur = queue.pop)
        
        pos = [cur[:x], cur[:y]]
        @accessibility[pos] ||= {back: []}
        @accessibility[pos][:back].concat(cur[:back])
        [U, R, D, L].each do |dir|
          x = {U => pos[0], R => pos[0]+1, D => pos[0], L => pos[0]-1}[dir]
          y = {U => pos[1]-1, R => pos[1], D => pos[1]+1, L => pos[1]}[dir]
          
          next if visited.find{|v| v[:x] == x and v[:y] == y}
          
          room1 = get_room(*pos)
          room2 = get_room(x, y)
          next if not room1 == room2 || room1.get_exit(dir, {U => x-room1.x, R => y-room1.y, D => x-room1.x, L => y-room1.y}[dir])
          
          queue << {x: x, y: y, back: [OPPOSITE[dir]]}
        end
      end
    end
  end
  
  def update_save_distances(map)
    save = @save_points.last
    
    queue = [{x: save.x, y: save.y, dist: 0}]
    visited = []
    
    while !queue.empty?
      visited << (cur = queue.pop)
      
      pos = [cur[:x], cur[:y]]
      if map[pos]
        map[pos] = [map[pos], cur[:dist]].min
      else
        map[pos] = cur[:dist]
      end
      
      [U, R, D, L].each do |dir|
        x = {U => pos[0], R => pos[0]+1, D => pos[0], L => pos[0]-1}[dir]
        y = {U => pos[1]-1, R => pos[1], D => pos[1]+1, L => pos[1]}[dir]
        
        next if visited.find{|v| v[:x] == x and v[:y] == y}
        
        room1 = get_room(*pos)
        room2 = get_room(x, y)
        next if not room1 == room2 || room1.get_exit(dir, {U => x-room1.x, R => y-room1.y, D => x-room1.x, L => y-room1.y}[dir])
        
        queue << {x: x, y: y, dist: cur[:dist]+1}
      end
    end
  end
  
  def is_back?(x, y, dir, abilities)
    @accessibility[[x, y]][:back].include?(dir) and @accessibility[[x, y]][:back].uniq.length == 1
  end
  
  def get_location(area)
    @locations.find{|loc| loc.base.areas.include?(area.type)}
  end
  
  def last_key
    @keys.empty? ? {x: 0, y: 0} : @keys.last
  end
  
  def map_distance(x1, y1, x2, y2)
    (x1-x2).abs + (y1-y2).abs
  end
end

class Location
  attr_reader :name
  
  @@colors = {}
  
  def initialize(id)
    @id = id
    
    @name = generate_name
  end
  
  def get_color
    @@colors[self] ||= Color.new(*base.color)
  end
  
  def base
    LocationBase.get(@id)
  end
end

class Area
  attr_accessor :x, :y, :w, :h, :privilege, :location, :previous
  attr_reader :tileset, :type
  
  def empty
    @empty and $world.rooms.length < World::PREFERRED_ROOM_COUNT
  end
  
  def base
    AreaBase.get(@type)
  end
end

class Room
  TILE = 25
  UNVISITED_TILE_COLOR = 0xff0000ff
  TILE_SEPARATOR = 0xff4040ff
  TILE_BORDER = 0xffffc100
  UNVISITED_BORDER = 0xff8ba7ff
  
  attr_accessor :x, :y, :w, :h, :origin, :save_point, :restrict_exits
  attr_reader :segments, :area, :unique_id, :placeholders, :exits, :type
  
  def draw
    @w.times do |x|
      @h.times do |y| next if !$save[:discovered].include?([@x+x ,@y+y])
        visited = true
        tls('System/Map',TILE, TILE, retro: true)[0].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,1,0,0.5,0.5,1,1, visited ? @area.location.get_color : UNVISITED_TILE_COLOR)
        tls('System/Map',TILE)[5].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,1,0) if @save_point
        tls('System/Map',TILE)[1].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,1,0,0.5,0.5,1,1,TILE_SEPARATOR)
        tls('System/Map',TILE)[2+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,3,0,0.5,0.5,1,1,TILE_BORDER) if x==0 and y==0
        tls('System/Map',TILE)[2+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,3,90,0.5,0.5,1,1,TILE_BORDER) if x==@w-1 and y==0
        tls('System/Map',TILE)[2+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,3,180,0.5,0.5,1,1,TILE_BORDER) if x==@w-1 and y==@h-1
        tls('System/Map',TILE)[2+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,3,270,0.5,0.5,1,1,TILE_BORDER) if x==0 and y==@h-1
        tls('System/Map',TILE)[(visited and get_exit(U,x)) ? 4 : 3+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,2,0,0.5,0.5,1,1,TILE_BORDER) if y==0
        tls('System/Map',TILE)[(visited and get_exit(R,y)) ? 4 : 3+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,2,90,0.5,0.5,1,1,TILE_BORDER) if x==@w-1
        tls('System/Map',TILE)[(visited and get_exit(D,x)) ? 4 : 3+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,2,180,0.5,0.5,1,1,TILE_BORDER) if y==@h-1
        tls('System/Map',TILE)[(visited and get_exit(L,y)) ? 4 : 3+(visited ? 0 : 3)].draw_rot((@x+x)*TILE+TILE/2.0,(@y+y)*TILE+TILE/2.0,2,270,0.5,0.5,1,1,TILE_BORDER) if x==0
      end
    end
  end
  
  def add_exit(direction, position, add_to_world)
    return if get_exit(direction, position)
    exit = Exit.new(direction, position)
    @exits << exit
    add_to_world << [@exits.last, self] if add_to_world
    exit
  end
  
  def get_exit(direction, position)
    @exits.find do |e|
      e.direction == direction and e.position == position
    end
  end
  
  def get_pattern(x, y, direction)
    case direction
      when U
      values = [y == 0,    @x+x,  @y-1,  D, x,   y-1, x]
      when R
      values = [x == @w-1, @x+@w, @y+y,  L, x+1, y,   y]
      when D
      values = [y == @h-1, @x+x,  @y+@h, U, x,   y+1, x]
      when L
      values = [x == 0,    @x-1,  @y+y,  R, x-1, y,   y]
    end
    
    if values[0]
      exit = get_exit(direction, values[6])
      exit.pattern if exit
    else
      segment = get_segment(values[4], values[5])
      segment.get_pattern(values[3]) if segment
    end
  end
  
  def get_segment(x, y)
    @segments[[x,y]]
  end
  
  def replace_segment(x, y, segment)
    @segments[[x,y]] = segment
  end
  
  def get_matching_segments(up, right, down, left, is_border)
    base.segments.reject do |id|
      segment = SegmentBase.get(id)
      segment.has_modifier?("no top") && is_border[U] or
      segment.has_modifier?("no bottom") && is_border[D] or
      
      up && !segment.get_connections(U).find{|c| c.type == C_BOTH || ((c.type == C_EXIT) == is_border[U]) and up == true || c.get_pattern(U) == up} or
      right && !segment.get_connections(R).find{|c| c.type == C_BOTH || ((c.type == C_EXIT) == is_border[R]) and right == true || c.get_pattern(R) == right} or
      down && !segment.get_connections(D).find{|c| c.type == C_BOTH || ((c.type == C_EXIT) == is_border[D]) and down == true || c.get_pattern(D) == down} or
      left && !segment.get_connections(L).find{|c| c.type == C_BOTH || ((c.type == C_EXIT) == is_border[L]) and left == true || c.get_pattern(L) == left} or
      
      !down && !is_border[D] && !segment.get_connections(D).find{|c| c.type != C_EXIT} or
      !right && !is_border[R] && !segment.get_connections(R).find{|c| c.type != C_EXIT}
    end
  end
  
  def read_grid_tile(x, y)
    @cache ||= {}
    Map.tile_type(if !@cache[[x, y]]
      @cache[[x, y]] = @segments[[x/SegmentBase::WIDTH, y/SegmentBase::HEIGHT]].get_grid(self, x/SegmentBase::WIDTH, y/SegmentBase::HEIGHT)
    else
      @cache[[x, y]]
    end[x%SegmentBase::WIDTH + y%SegmentBase::HEIGHT*SegmentBase::WIDTH])
  end
  
  def space_for_save(x, y)
    4.times do |dx|
      4.times do |dy|
        return if not dy < 3 && read_grid_tile(x+dx, y+dy) == :air || dy == 3 && read_grid_tile(x+dx, y+dy) == :wall
      end
    end
    true
  end
  
  def clear_cache
    remove_instance_variable(:@cache) if instance_variable_defined?(:@cache)
    remove_instance_variable(:@origin) if instance_variable_defined?(:@origin)
  end
  
  def base
    RoomBase.get(@type)
  end
end

class Segment
  attr_reader :connections
  
  def initialize(id)
    @id = id
    @connections = [-1] * 4
  end
  
  def get_grid(room, x, y)
    grid = base.grid.dup
    apply_smart_corners(grid, room, x, y) if base.has_modifier? "smart corners"
    
    order = [U, R, D, L]
    @connections.each.with_index{|id, dir| if id != -1 and base.get_connections(dir)[id].has_modifier?("last")
      order.delete(dir)
      order.push(dir)
      break
    end}
    @connections.each.with_index{|id, dir| if id != -1 and base.get_connections(dir)[id].has_modifier?("first")
      order.delete(dir)
      order.unshift(dir)
      break
    end}
    order.each{|ord| merge_connection(grid, ord)}
    
    grid
  end
  
  def get_layer_grid(layer)
    grid = layer.grid.dup
    merge_connection_special(grid, U)
    merge_connection_special(grid, R)
    merge_connection_special(grid, D)
    merge_connection_special(grid, L)
    grid
  end
  
  def merge_connection(grid, direction)
    return grid if @connections[direction] == -1
    connection_grid = base.get_connections(direction)[@connections[direction]].grid
    
    grid.each_index do |i|
      grid[i] = connection_grid[i] if connection_grid[i]
    end
  end
  
  def merge_connection_special(grid, direction)
    return grid if @connections[direction] == -1
    connection_grid = base.get_connections(direction)[@connections[direction]].grid
    
    grid.each_index do |i|
      grid[i] = (grid[i] and !connection_grid[i])
    end
  end
  
  def get_pattern(direction)
    if @connections[direction] > -1
      base.get_connections(direction)[@connections[direction]].get_pattern(direction)
    end
  end
  
  def apply_smart_corners(grid, room, x, y)
    grid[0] = 0 if x > 0 and y > 0
    grid[SegmentBase::WIDTH-1] = 0 if x < room.w-1 and y > 0
    grid[SegmentBase::HEIGHT * SegmentBase::WIDTH - 1] = 0 if !base.has_modifier?("smart corner upper") and x < room.w-1 and y < room.h-1
    grid[(SegmentBase::HEIGHT-1) * SegmentBase::WIDTH] = 0 if !base.has_modifier?("smart corner upper") and x > 0 and y < room.h-1
  end
  
  def base
    SegmentBase.get(@id)
  end
end

class Exit
  attr_accessor :pattern
  attr_reader :direction, :position
  
  def initialize(direction, position)
    @direction, @position = direction, position
  end
end